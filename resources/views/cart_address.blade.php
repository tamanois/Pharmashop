﻿@extends('sidebar-skeleton')


@section('content')


	<div class="row-fluid">
		<div class="col-md-8">
			<form action="{{route('proceed_payment')}}" method="post" id="payment-address-form">
				{{csrf_field()}}
				<div class="address-container">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3>Shipping address</h3>
						</div>
						<div class="panel-body">
							<div class="col-md-12">
								<div class="col-md-6 col-sm-6">
									<p><strong>Country</strong></p>
									<h4 style="color: grey;padding-left: 10px" id="sshipping_country"> {{$ship_country->name}}</h4>
									<input type="hidden" name="shipping_country" id="shipping_country"  value="{{$ship_country->id}}" >
								</div>
								<div class="col-md-6 col-sm-6">
									<p><strong>City</strong></p>
									<h4 style="color: grey;padding-left: 10px" id="sshipping_city">@if(isset($shipping_address)) {{$shipping_address->city}} @elseif(old('shipping_city')) {{old('shipping_city')}} @else Empty @endif</h4>
									<input type="hidden" name="shipping_city" id="shipping_city" @if(isset($shipping_address)) value="{{$shipping_address->city}}" @else value="{{old('shipping_city')}}" @endif>

								</div>

								<div class="col-md-6 col-sm-6">
									<p><strong>Address</strong></p>
									<h4 style="color: grey;padding-left: 10px" id="sshipping_address">@if(isset($shipping_address)) {{$shipping_address->address}} @elseif(old('shipping_address')) {{old('shipping_address')}} @else Empty @endif</h4>
									<input type="hidden" name="shipping_address" id="shipping_address" @if(isset($shipping_address)) value="{{$shipping_address->address}}" @else value="{{old('shipping_address')}}" @endif>

								</div>
								<div class="col-md-6 col-sm-6">
									<p><strong>Postal code</strong></p>
									<h4 style="color: grey;padding-left: 10px" id="sshipping_postal_code">@if(isset($shipping_address)) {{$shipping_address->postal_code}} @elseif(old('shipping_postal_code')) {{old('shipping_postal_code')}} @else Empty @endif</h4>
									<input type="hidden" name="shipping_postal_code" id="shipping_postal_code" @if(isset($shipping_address)) value="{{$shipping_address->postal_code}}" @else value="{{old('shipping_postal_code')}}" @endif>

								</div>
								<div class="col-md-6">
									<p><strong>Phone</strong></p>
									<h4 style="color: grey;padding-left: 10px" id="sshipping_phone">@if(isset($shipping_address)) {{$shipping_address->phone}} @elseif(old('shipping_phone_number')) {{old('shipping_phone_number')}} @else Empty @endif</h4>
									<input type="hidden" name="shipping_phone_number" id="shipping_phone_number" @if(isset($shipping_address)) value="{{$shipping_address->phone}}" @else value="{{old('shipping_phone_number')}}" @endif>

								</div>

								<div class="col-md-12">
									<a href="#edit_shipping" data-toggle="modal" data-backdrop="false" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-edit"></span> Edit</a>
								</div>

							</div>
						</div>
					</div>

					<div class="panel panel-info">
						<div class="panel-heading">
							<h3>Billing address</h3>
						</div>
						<div class="panel-body">
							@if(!Auth::check())
							<div >
								<p><strong>E-mail address :</strong></p>
								<h4 style="color: grey;padding-left: 10px" id="semail">@if(isset($email)) {{$email}} @else Empty @endif</h4>
								<input type="hidden" name="email" id="email"  @if(isset($email)) value="{{$email}}" @elseif(old('email')) {{old('email')}} @endif >

							</div>
							@endif

							<div class="col-md-6 col-sm-6">
								<p><strong>Country</strong></p>
								<h4 style="color: grey;padding-left: 10px" id="sbilling_country">@if(isset($billing_address)) {{$billing_address->country->name}} @elseif(old('billing_country')) {{$countries->where('id','=',old('billing_country'))->first()->name}} @else Empty @endif</h4>
								<input type="hidden" name="billing_country" id="billing_country"  @if(isset($billing_address)) value="{{$billing_address->country->id}}" @else value="{{old('billing_country')}}" @endif >

							</div>
							<div class="col-md-6 col-sm-6">
								<p><strong>City</strong></p>
								<h4 style="color: grey;padding-left: 10px" id="sbilling_city">@if(isset($billing_address)) {{$billing_address->city}} @elseif(old('billing_city')) {{old('billing_city')}} @else Empty @endif</h4>
								<input type="hidden" name="billing_city" id="billing_city" @if(isset($billing_address)) value="{{$billing_address->city}}" @else value="{{old('billing_city')}}" @endif>

							</div>


							<div class="col-md-6 col-sm-6">
								<p><strong>Address</strong></p>
								<h4 style="color: grey;padding-left: 10px" id="sbilling_address">@if(isset($billing_address)) {{$billing_address->address}} @elseif(old('billing_address')) {{old('billing_address')}} @else Empty @endif</h4>
								<input type="hidden" name="billing_address" id="billing_address" @if(isset($billing_address)) value="{{$billing_address->address}}" @else value="{{old('billing_address')}}" @endif>

							</div>
							<div class="col-md-6 col-sm-6">
								<p><strong>Postal code</strong></p>
								<h4 style="color: grey;padding-left: 10px" id="sbilling_postal_code">@if(isset($billing_address)) {{$billing_address->postal_code}} @elseif(old('billing_postal_code')) {{old('billing_postal_code')}} @else Empty @endif</h4>
								<input type="hidden" name="billing_postal_code" id="billing_postal_code" @if(isset($billing_address)) value="{{$billing_address->postal_code}}" @else value="{{old('billing_postal_code')}}" @endif>

							</div>

							<div class="col-md-12">
								<a href="#edit_billing" data-toggle="modal" data-backdrop="false" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-edit"></span> Edit</a>
							</div>
						</div>
					</div>
				</div>

			</form>

		</div>
		<div class="col-md-4">
			<div class="summary">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="text-center">Cart</h3>
					</div>
					<div class="panel-body">
						<h4 class="text-center"> <span class="text-gray"> Purchased({{$nbItems}} items) : </span> <span id="purchase_price">{{round($price,2)}}</span> @lang('main.currency')</h4>
						<hr>
						<h4 class="text-center"> <span class="text-gray">Shipping country: </span> <a href="#choose_ship_country" data-toggle="modal" data-backdrop="false" id="ship_country_name" style="text-decoration: underline">{{$ship_country->name}}</a></h4>
						<h4 class="text-center"> <span class="text-gray">Price: </span> <span id="ship_price">{{$ship_country->ship_price}}</span> @lang('main.currency')</h4>
						<hr>

						<h4 class="text-center alert alert-info" style="font-size: 20px"> <span class="text-gray">Total: </span> <span id="total_price">{{round($ship_country->ship_price+$price,2)}}</span> @lang('main.currency')</h4>

						<div class="text-center">
							<span class="btn btn-lg btn-warning" onclick="submitAddress()"><span class="glyphicon glyphicon-ok-sign"></span> Proceed payment</span>

						</div>


					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="modal fade" id="edit_billing">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title">Please fill the fields</h4>
				</div>
				<div
						class="modal-body">



					<form accept-charset="UTF-8" role="form" id="edit-billing-form" method="POST" action="#">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<fieldset>
							<div class="row-fluid">

								@if(!Auth::check())
								<div class="form-group col-md-12">


									<label>E-mail address : </label>
									<input class="form-control" @if(isset($email)) value="{{$email}}" @else value="{{old('email')}}" @endif id="edit-email" name="email" type="email"   required>


								</div>
								@endif
								<div class="form-group col-md-6">


									<label>Country : </label>
									<select name="country" class="form-control" form="edit-billing-form" required id="edit-bcountry">
										@foreach($countries as $c)
											<option value="{{$c->id}}" @if(isset($billing_address)) @if($billing_address->country->id==$c->id) selected @endif @else @if(old('billing_country')==$c->id) selected @endif  @endif>{{$c->name}}</option>
										@endforeach
									</select>

								</div>

								<div class="form-group col-md-6">
									<label >City : </label>
									<input class="form-control" @if(isset($billing_address)) value="{{$billing_address->city}}" @else value="{{old('billing_city')}}" @endif id="edit-bcity" name="city" type="text"   required>

								</div>

								<div class="form-group col-md-6">
									<label >Address : </label>
									<input class="form-control" @if(isset($billing_address)) value="{{$billing_address->address}}" @else value="{{old('billing_address')}}" @endif id="edit-baddress" name="address" type="text"   required>

								</div>
								<div class="form-group col-md-6">
									<label >Postal code : </label>
									<input class="form-control"  @if(isset($billing_address)) value="{{$billing_address->postal_code}}" @else value="{{old('billing_postal_code')}}" @endif id="edit-bpostal_code" name="postal_code" type="text" required>

								</div>
							</div>

							<button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


						</fieldset>
					</form>


				</div>





			</div>


		</div>
	</div>

	<div class="modal fade" id="edit_shipping">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title">Please fill the fields</h4>
				</div>
				<div class="modal-body">

					<form accept-charset="UTF-8" role="form" id="edit-shipping-form" method="POST" action="#">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<fieldset>
							<div class="row-fluid">



								<div class="form-group col-md-6">
									<label >City : </label>
									<input class="form-control" @if(isset($shipping_address)) value="{{$shipping_address->city}}" @else value="{{old('shipping_city')}}"  @endif id="edit-scity"  name="city" type="text"  required>

								</div>

								<div class="form-group col-md-6">
									<label >Address : </label>
									<input class="form-control" @if(isset($shipping_address)) value="{{$shipping_address->address}}" @else value="{{old('shipping_address')}}" @endif id="edit-saddress" name="address" type="text"   required>

								</div>
								<div class="form-group col-md-6">
									<label >Postal code : </label>
									<input class="form-control"  @if(isset($shipping_address)) value="{{$shipping_address->postal_code}}" @else value="{{old('shipping_postal_code')}}" @endif id="edit-spostal_code"  name="postal_code" type="text" required>

								</div>

								<div class="form-group col-md-6">
									<label >Phone : </label>
									<input class="form-control"  @if(isset($shipping_address)) value="{{$shipping_address->phone}}" @else value="{{old('shipping_phone')}}" @endif id="edit-sphone" name="phone" type="text" required>

								</div>
							</div>

							<button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="choose_ship_country">
		<div class="modal-dialog" >
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title">Select a country</h4>
				</div>
				<div class="modal-body" style="height: 150px">
					<label >Country</label>
					<div class="form-group">
						<select name="country" class="form-control" id="ship_country" style="padding-top: 15px; padding-bottom: 15px;height: 50px;" >
							@foreach($countries as $c)
								@if($c->ship==1)
									<option id="sp{{$c->id}}" value="{{$c->ship_price}}">{{$c->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="btn btn-primary pull-right" data-dismiss="modal" onclick="setShipPrice();"><span class="glyphicon glyphicon-ok-sign"></span> OK</div>



				</div>

			</div>


		</div>
	</div>

@endsection

@section('styles')
	<style>
		#header
		{
			margin-top: -40px !important;
		}
	</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>

        function evaluateTotal() {
			var total=0;
            var pp=$('#purchase_price');
            var sp=$('#ship_price');

            var ppv=parseFloat(pp.html());
            var spv=parseFloat(sp.html());

            total=ppv+spv;
            $('#total_price').html(total.toFixed(2));

        }
        function setShipPrice() {
            var sc=$('#ship_country');
		    var v=sc.val();
		    var el=sc[0];
		    var elInput=$('#shipping_country');
		    var name=el.options[el.selectedIndex].innerHTML;
            var id=el.options[el.selectedIndex].id;
            id= id.substring(2);
            id= parseInt(id);
            elInput.val(id);
            $('#ship_price').html(v);
		    $('#ship_country_name').html(name);
            $('#sshipping_country').html(name);


            evaluateTotal();
        }



        function submitAddress(){
			$('#payment-address-form').submit();
		}
		$(function () {
			$('#edit-billing-form').on('submit',function (e) {
				e.preventDefault();
				$('#edit_billing').modal('hide');
				var co=$('#edit-bcountry');
				var el=co[0];
                var coname=el.options[el.selectedIndex].innerHTML;
                $('#billing_country').val(co.val());
                $('#sbilling_country').html(coname);

                var add=$('#edit-baddress');
                var ci=$('#edit-bcity');
                var pc=$('#edit-bpostal_code');


                $('#billing_address').val(add.val());
                $('#sbilling_address').html(add.val());

                $('#billing_city').val(ci.val());
                $('#sbilling_city').html(ci.val());

                $('#billing_postal_code').val(pc.val());
                $('#sbilling_postal_code').html(pc.val());

                var em=$('#edit-email');

                $('#email').val(em.val());
                $('#semail').html(em.val());

            })
        })
        $(function () {
            $('#edit-shipping-form').on('submit',function (e) {
                e.preventDefault();
                $('#edit_shipping').modal('hide');


                var add=$('#edit-saddress');
                var ci=$('#edit-scity');
                var ph=$('#edit-sphone');
                var pc=$('#edit-spostal_code');

                $('#shipping_address').val(add.val());
                $('#sshipping_address').html(add.val());

                $('#shipping_city').val(ci.val());
                $('#sshipping_city').html(ci.val());

                $('#shipping_phone_number').val(ph.val());
                $('#sshipping_phone').html(ph.val());

                $('#shipping_postal_code').val(pc.val());
                $('#sshipping_postal_code').html(pc.val());
            })
        })

	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection