﻿@extends('skeleton')


@section('d-content')

	<div class="container-fluid">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="text-center">
                            <h2 class=""><strong>Order #{{sprintf('%08d',$order->id)}}</strong></h2>
                        </div>
                    </div>

                </div>

                <div class="panel-body">
                    @if($order->status==5)
                        <a class="btn btn-warning pull-right" href="{{route('show_bill').'?id='.$order->id}}"><strong><span class="glyphicon glyphicon-file"></span> Order bill</strong></a>
                    @endif
                    <h3>STATUS:  <span class="alert @if($order->status==0) alert-warning @elseif($order->status==5) alert-success @else alert-danger @endif" style="">
                            <strong class="text-uppercase">
                            @if($order->status==0)
                                    Pending
                                @elseif($order->status==5)
                                    Paid
                                @else
                                    Failed
                                @endif
                            </strong>
                        </span></h3>
                    <h3 class="text-uppercase">Payment method: <span class="text-gray">@if($order->payment_url=='card_payment') Credit card @else CryptoCurrency @endif</span></h3>
                    <h3 style="margin-top: 20px">Date: <span class="text-gray">{{(new DateTime($order->created_at))->format('H:i, d-m-Y')}}</span></h3>

                    <div class="row-fluid">
                        <div class="col-md-6 col-sm-6">
                            <legend>Billing</legend>
                            <div class="text-gray" style="font-size: 18px">
                                <p>{{$order->billing->address.', '.$order->billing->city.', '.$order->billing->country->name}}</p>
                                <p>P.O BOX: {{$order->billing->postal_code}}</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <legend>Shipping</legend>
                            <div class="text-gray" style="font-size: 18px">
                                <p>{{$order->shipping->address.', '.$order->shipping->city.', '.$order->shipping->country->name}}</p>
                                <p>P.O BOX: {{$order->shipping->postal_code}}</p>
                                <p>Tel: {{$order->shipping->phone}}</p>
                            </div>
                        </div>
                    </div>

                    <h3 class="text-uppercase"> Products details</h3>
                    <table class="table table-stripped table-responsive">
                        <thead class="thead-inverse">
                        <tr>
                            <th>Description</th>
                            <th>price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($order->order_items as $oi)
                            <tr>
                                <td><strong>{{$oi->dosage->product->name}}- <span class="text-gray">{{$oi->dosage->description}}</span>   </strong></td>
                                <td><strong>{{$oi->price}} @lang('main.currency')</strong></td>
                                <td>{{$oi->quantity}}</td>
                                <td><strong>{{round($oi->price*$oi->quantity,2)}} @lang('main.currency')</strong></td>
                            </tr>

                        @endforeach
                        <tr>
                            <td colspan="3"><strong class="text-uppercase">SHIP TO {{$order->shipping->country->name}}</strong></td>
                            <td><strong>{{$order->shipping->country->ship_price}} @lang('main.currency')</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"><strong>TOTAL</strong></td>
                            <td><strong class="text-primary">{{$order->amount}} @lang('main.currency')</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="text-center">
                        <h3 class="text-uppercase" > Order Amount: <span class="text-primary">{{$order->amount}} @lang('main.currency') @if($order->payment_url!='card_payment') ({{$order->pay_amount.' '.$order->currency}}) @endif</span></h3>

                        @if($order->status==5)
                            <div style="margin-top: 20px">
                                @if($order->checked==1) <h4 class="btn btn-success disabled">Order Delivered</h4> @else <h4 class="btn btn-danger disabled">Not yet delivered</h4> @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>

    </div>

@endsection

@section('styles')

    	<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
		</style>

	@yield('styles')
@endsection

@section('scripts')
	<script>



	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection