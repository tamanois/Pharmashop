@extends('account')
@section('account-section')

    <div class="row-fluid">
        <div class="col-md-12">
            <legend>Billing Address</legend>
        </div>
        <div class="col-md-6 col-sm-6">
            <p><strong>Country</strong></p>
            <h4 style="color: grey;padding-left: 10px">@if($billing_address) {{$billing_address->country->name}} @endif</h4>
        </div>
        <div class="col-md-6 col-sm-6">
            <p><strong>City</strong></p>
            <h4 style="color: grey;padding-left: 10px">@if($billing_address) {{$billing_address->city}} @endif</h4>
        </div>


        <div class="col-md-6 col-sm-6">
            <p><strong>Address</strong></p>
            <h4 style="color: grey;padding-left: 10px">@if($billing_address) {{$billing_address->address}} @endif</h4>
        </div>
        <div class="col-md-6 col-sm-6">
            <p><strong>Postal code</strong></p>
            <h4 style="color: grey;padding-left: 10px">@if($billing_address) {{$billing_address->postal_code}} @endif</h4>
        </div>



        <div class="col-md-12">
            <a href="#edit_billing" data-toggle="modal" data-backdrop="false" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-edit"></span> Edit</a>
        </div>
    </div>


<div class="row-fluid" style="margin-top: 20px">
    <div class="col-md-12">
        <legend>Shipping Address</legend>
    </div>
    <div class="col-md-6 col-sm-6">
        <p><strong>Country</strong></p>
        <h4 style="color: grey;padding-left: 10px">@if($shipping_address) {{$shipping_address->country->name}} @endif</h4>
    </div>
    <div class="col-md-6 col-sm-6">
        <p><strong>City</strong></p>
        <h4 style="color: grey;padding-left: 10px">@if($shipping_address) {{$shipping_address->city}} @endif</h4>
    </div>

    <div class="col-md-6 col-sm-6">
        <p><strong>Address</strong></p>
        <h4 style="color: grey;padding-left: 10px">@if($shipping_address) {{$shipping_address->address}} @endif</h4>
    </div>
    <div class="col-md-6 col-sm-6">
        <p><strong>Postal code</strong></p>
        <h4 style="color: grey;padding-left: 10px">@if($shipping_address) {{$shipping_address->postal_code}} @endif</h4>
    </div>
    <div class="col-md-6">
        <p><strong>Phone</strong></p>
        <h4 style="color: grey;padding-left: 10px">@if($shipping_address) {{$shipping_address->phone}} @endif</h4>
    </div>


    <div class="col-md-12">
        <a href="#edit_shipping" data-toggle="modal" data-backdrop="false" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-edit"></span> Edit</a>
    </div>
</div>


    <div class="modal fade" id="edit_billing">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Please fill the fields</h4>
                </div>
                <div
                        class="modal-body">



                    <form accept-charset="UTF-8" role="form" id="edit-billing-form" method="POST" action="{{route('update_billing')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <fieldset>
                            <div class="row-fluid">
                                <div class="form-group col-md-6">
                                    <label>Country : </label>
                                    <select name="country" class="form-control" form="edit-billing-form">
                                        @foreach($countries as $c)
                                            <option value="{{$c->id}}" @if($billing_address) @if($billing_address->country->id==$c->id) selected @endif @endif>{{$c->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group col-md-6">
                                    <label >City : </label>
                                    <input class="form-control" @if($billing_address) placeholder="{{$billing_address->city}}" @endif  name="city" type="text"   >

                                </div>

                                <div class="form-group col-md-6">
                                    <label >Address : </label>
                                    <input class="form-control" @if($billing_address) placeholder="{{$billing_address->address}}" @endif name="address" type="text"   >

                                </div>
                                <div class="form-group col-md-6">
                                    <label >Postal code : </label>
                                    <input class="form-control"  @if($billing_address) placeholder="{{$billing_address->postal_code}}" @endif  name="postal_code" type="text" >

                                </div>
                            </div>

                            <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                        </fieldset>
                    </form>


                </div>





            </div>


        </div>
    </div>

        <div class="modal fade" id="edit_shipping">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div class="modal-body">

                        <form accept-charset="UTF-8" role="form" id="edit-shipping-form" method="POST" action="{{route('update_shipping')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>
                                <div class="row-fluid">
                                    <div class="form-group col-md-6">
                                        <label>Country : </label>
                                        <select name="country" class="form-control" form="edit-shipping-form">
                                            @foreach($scountries as $c)
                                                <option value="{{$c->id}}" @if($shipping_address) @if($shipping_address->country->id==$c->id) selected @endif @endif>{{$c->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label >City : </label>
                                        <input class="form-control" @if($shipping_address) placeholder="{{$shipping_address->city}}" @endif  name="city" type="text"   >

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label >Address : </label>
                                        <input class="form-control" @if($shipping_address) placeholder="{{$shipping_address->address}}" @endif name="address" type="text"   >

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Postal code : </label>
                                        <input class="form-control"  @if($shipping_address) placeholder="{{$shipping_address->postal_code}}" @endif  name="postal_code" type="text" >

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label >Phone : </label>
                                        <input class="form-control"  @if($shipping_address) placeholder="{{$shipping_address->phone}}" @endif  name="phone" type="text" >

                                    </div>
                                </div>

                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

@endsection