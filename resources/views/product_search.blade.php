﻿@extends('sidebar-skeleton')


@section('content')
	<div >

		<div class="row thumbnails">
			<?php $n=0; ?>
			@foreach($products as $p)

				<div class="col-md-4 col-sm-6">
					<div class="thumbnail">
						@if(date_diff((new DateTime()),(DateTime::createFromFormat('Y-m-d H:i:s',$p->created_at)))->days<=15)
							<i class="tag"></i>

						@endif
						<a  href="{{route('product_details',$p->id)}}">
							<div style="padding: 10px;border-radius:5px;border:#ddd solid 1px;width: 120px;margin-left: auto;margin-right: auto" >
								<img src="{{asset($p->imgurl)}}" alt=""/>

							</div>
						<div class="caption">
							<h5 style="margin-bottom: 0;font-weight: 600"><a href="{{route('product_details',$p->id)}}" class="text-uppercase"> {{$p->name}}</a></h5>
							<p class="text-gray" style="font-size: 11px;margin-bottom: 5px">
								@if(isset($p->generic_name))
									{{$p->generic_name}}
									@else
										<span style="color: rgba(255,255,255,0)">rien</span>
								@endif
							</p>
								<div class="center-block">
									<div style="margin-left: auto;margin-right:auto;max-width: 200px">
										<div class="small text-gray">
											Strength
										</div>
									</div>
									<select  id="dosage{{$n}}" class="form-control " style="margin-left: auto;margin-right:auto;height: auto;font-size: 14px;max-width: 200px" onchange="dosageChange(this,'price{{$n}}')">
										@foreach($p->dosages as $d)

											<option id="{{$d->id}}" value="{{$d->price}}">{{$d->description}}</option>

										@endforeach
									</select>

								</div>





							<h4 style="text-align:center">
								<a class="btn btn-success" onclick="showCcartModal('dosage{{$n}}')" style="margin-bottom: 2px">Add to <i class="glyphicon glyphicon-shopping-cart"></i></a>
								<a class="btn disabled btn-default" href="#" id="price{{$n}}" style="font-weight: 600">{{($price=$p->dosages->first())?$price->price:0}} @lang('main.currency')</a></h4>
						</div>
					</div>
				</div>
				<?php $n++; ?>
			@endforeach


		</div>

	</div>
@endsection

@section('styles')
	<style>
		#header
		{
			margin-top: -40px !important;
		}
	</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>
		function dosageChange(el,priceEl) {
		    var v=el.value;
		    $('#'+priceEl).html(v+' €');
        }


	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection