﻿@extends('skeleton')


@section('d-content')
<div class="">
	<h2 class=" "><strong>VISIT US</strong></h2>
    <hr style="margin-top: 0">

</div>
	<div class="contact-container">

        <div class="container-fluid">
            <div class="col-md-4">
                <div class="">
                    <h4><strong>Contact Details</strong></h4>
                    <p>	18 Fresno,<br/> CA 93727, USA
                        <br/><br/>
                        info@pharmashop.com<br/>
                        ﻿Tel 123-456-6780<br/>
                        Fax 123-456-5679<br/>
                        web:pharmashop.com
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="">
                    <h4><strong>Opening Hours</strong></h4>
                    <h5> <strong>Monday - Friday</strong></h5>
                    <p>09:00am - 09:00pm<br/><br/></p>
                    <h5><strong>Saturday</strong></h5>
                    <p>09:00am - 07:00pm<br/><br/></p>
                    <h5><strong>Sunday</strong></h5>
                    <p>12:30pm - 06:00pm<br/><br/></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="">
                    <h4><strong>Email Us</strong></h4>
                    <form class="form form-horizontal" id="contact-form" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="errors-field alert alert-danger" style="display: none;">
                            <ul>

                            </ul>
                        </div>
                        <div class="success-field alert alert-success " style="display: none;">

                        </div>
                        <fieldset>
                             <div class="form-group">

                                <input type="text" placeholder="name" name="name" class="contact form-control"  required/>

                            </div>
                            <div class="form-group">

                                <input type="email" placeholder="email" name="email" class="contact form-control" required/>

                            </div>
                            <div class="form-group">

                                <input type="text" placeholder="subject" name="subject" class=" contact form-control" required/>

                            </div>
                            <div class="form-group">
                                <textarea rows="3" id="textarea" name="message" class="contact form-control" required></textarea>

                            </div>

                            <button class="btn btn-success btn-lg" type="submit"><span class="glyphicon glyphicon-envelope"></span> Send Message</button>

                        </fieldset>
                    </form>
                </div>
            </div>




        </div>

        <div class="row">
            <div class="span12">
                <iframe style="width:100%; height:300; border: 0px" scrolling="no" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=18+California,+Fresno,+CA,+United+States&amp;aq=0&amp;oq=18+California+united+state&amp;sll=39.9589,-120.955336&amp;sspn=0.007114,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=18,+Fresno,+California+93727,+United+States&amp;t=m&amp;ll=36.732762,-119.695787&amp;spn=0.017197,0.100336&amp;z=14&amp;output=embed"></iframe><br />
                <small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=18+California,+Fresno,+CA,+United+States&amp;aq=0&amp;oq=18+California+united+state&amp;sll=39.9589,-120.955336&amp;sspn=0.007114,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=18,+Fresno,+California+93727,+United+States&amp;t=m&amp;ll=36.732762,-119.695787&amp;spn=0.017197,0.100336&amp;z=14" style="color:#0000FF;text-align:left">View Larger Map</a></small>
            </div>
        </div>

	</div>

@endsection

@section('styles')
		<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
		</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>
    $('#contact-form').on('submit',function (e) {
        var shtml=$('.success-field');
        var ehtml=$('.errors-field');
        ehtml.find('ul').empty();
        ehtml.css('display','none');
        shtml.css('display','none');

        e.preventDefault();

        startLoading();

        $.ajax({
           url:'{{route('send_contact_mail')}}',
            type:'POST',
            data:(new FormData($('#contact-form')[0])),
            cache: false,
            contentType: false,
            processData: false,
            success: function (data,status,xhr) {

            stopLoading();

                if(data.errors)
               {
                   var i=0;
                   var err=data.errors;

                   for(i=0;i<err.length;i++)
                   {
                       ehtml.find('ul').append('<li>'+ err[i] +'</li>');
                   }
                   ehtml.css('display','block');
               }
               else {

                    shtml.html('Your message has been sent');
                    shtml.css('display','block');
                    $('.contact.form-control').each(function () {
                        this.value='';
                    })

                }
            },
            error:function (xhr,status,error) {
               stopLoading();
               showAlert(error,1);

            }
            
        });
    })


	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection