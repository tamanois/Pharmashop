@extends('sidebar-skeleton')
@section('content')


    <div class="col-md-12">
        <div class="well" style="min-height: 500px">

            <form class="form" method="post" action="{{route('register_try')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-md-7" >
                    <h4>Your personal information</h4>

                    <div class="form-group">
                        <label  for="inputFname1">First name <sup>*</sup></label>
                            <input type="text" id="inputFname1" class="form-control" placeholder="First Name" name="name" value="{{old('name')}}">
                    </div>
                    <div class="form-group">
                        <label  for="inputLnam">Last name <sup>*</sup></label>
                            <input type="text" id="inputLnam" class="form-control" placeholder="Last Name" name="forname" value="{{old('forname')}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="input_email">Email <sup>*</sup></label>
                            <input type="email" id="input_email" class="form-control" placeholder="Email" name="email" value="{{isset($email)?$email:old('email')}}">
                    </div>
                    <div class="form-group">
                        <label  for="inputPassword1">Password <sup>*</sup></label>
                            <input type="password" id="inputPassword1" placeholder="Password" class="form-control" name="password">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputPassword1">Repeat password <sup>*</sup></label>
                        <input type="password" id="inputPassword1" placeholder="Password" class="form-control" name="password_confirmation">
                    </div>

                    <p><sup>*</sup>Required field	</p>
                    <div class="control-group">
                        <div class="controls">

                            <input class="btn btn-large btn-success" type="submit" value="Register" />
                        </div>
                    </div>


                </div>



            </form>
        </div>

    </div>
@endsection

@section('title')
    Register - Pharmashop
@endsection