@extends('sidebar-skeleton')
@section('content')


    <div class="col-md-12">
        <div class="well" style="min-height: 200px">

            <form class="form" method="post" action="{{route('send_reset_link')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-md-7" >
                    <h4>Enter your email address</h4>

                    <div class="form-group">
                            <input type="email" id="inputFname1" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
                    </div>

                    <div class="control-group">
                        <div class="controls">

                            <input class="btn btn-large btn-success" type="submit" value="Send reset link" />
                        </div>
                    </div>


                </div>



            </form>
        </div>

    </div>
@endsection

@section('title')
    {{$page_title}} - Pharmashop
@endsection