@extends('sidebar-skeleton')
@section('content')


    <div class="col-md-12">
        <div class="well" style="min-height: 400px">

            <form class="form" method="post" action="{{route('reset_password')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="token" value="{{ $token }}">


                <div class="col-md-7" >
                    <h4>Change Your password</h4>


                    <div class="form-group">
                        <label class="control-label" for="input_email">E-mail address </label>
                            <input type="email" id="input_email" class="form-control" placeholder="Email" name="email" value="{{isset($email)?$email:old('email')}}">
                    </div>
                    <div class="form-group">
                        <label  for="inputPassword1">Password </label>
                            <input type="password" id="inputPassword1" placeholder="Password" class="form-control" name="password">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputPassword1">Confirm password </label>
                        <input type="password" id="inputPassword1" placeholder="Password" class="form-control" name="password_confirmation">
                    </div>

                    <div class="control-group">
                        <div class="controls">

                            <input class="btn btn-large btn-success" type="submit" value="Reset password" />
                        </div>
                    </div>


                </div>



            </form>
        </div>

    </div>
@endsection

@section('title')
    {{$page_title}} - Pharmashop
@endsection