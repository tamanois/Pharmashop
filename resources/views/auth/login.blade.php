@extends('sidebar-skeleton')
@section('content')


    <div >

        <div class="col-md-6 col-sm-7">
            <div class="well">
                <h5>ALREADY REGISTERED ?</h5>
                <form method="post" action="{{route('login_try')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="control-label" for="inputEmail1">Email</label>
                            <input class="form-control"  type="email" id="inputEmail1" placeholder="Email" name="email" value="{{old('email')}}" autofocus>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputPassword1">Password</label>
                            <input type="password" class="form-control"  id="inputPassword1" name="password" placeholder="Password">
                    </div>
                    <div class="checkbox-inline">
                        <input type="checkbox"  name="remember" > <span class="text-gray"> Remember me</span>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                            <button type="submit" class="btn">Sign in</button> <a href="{{route('password.email')}}">Forget password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6 col-sm-7">
            <div class="well">
                <h5>CREATE YOUR ACCOUNT</h5><br/>
                Enter your e-mail address to create an account.<br/><br/><br/>
                <form action="{{route('register_check')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="control-label" for="inputEmail0">E-mail address</label>
                            <input class="form-control"  type="email" id="inputEmail0" placeholder="Email" name="email">
                    </div>
                    <div class="controls">
                        <button type="submit" class="btn block">Create Your Account</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('title')
    Login - Pharmashop
@endsection