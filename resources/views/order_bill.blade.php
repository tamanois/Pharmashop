<style>
    .thead-inverse th, .thead-inverse {
        color: #fff;
        background-color: #373a3c;
    }
    .myFamily
    {
        font-family: Verdana, Helvetica, "Gill Sans", sans-serif;

    }
    .table
    {
        width: 100%;
        border: solid 1px;
        font-family: Verdana, Helvetica, "Gill Sans", sans-serif;
        font-size: 12px;
    }
    .table thead tr th, .table tbody tr td
    {
        padding: 5px 10px;
    }
    .table-bordered , .table-bordered tbody tr
    {
        border: solid 1px;
    }

</style>
<div style="width: 100%;position: relative" class="myFamily">
    <div style="width: 100%;display: flex">
        <div style="width: 50%;float: left;">
            <p style="font-size: 28px">@lang('main.site_name')</p>
            <p style="color: grey">
                18 Fresno, CA 93727, USA <br>
                info@pharmashop.com <br>
                Tel 123-456-6780 <br>
                Fax 123-456-5679 <br>
                web: pharmashop.com <br>
            </p>
        </div>
        <div style="width: 50%;float: right">
            <h2 style="margin-bottom: 0;padding-bottom: 5px;font-size: 28px;color: #D33800;float: right;">Bill B{{sprintf('%08d',$order->id)}}</h2>
            <p style="color: grey;font-size: 24px;float: right;margin-top: 50px">{{(new DateTime($order->updated_at))->format('d M Y')}}</p>
            <p style="color: grey;float: right;margin-top: 95px">
                @if(isset($user))
                    <strong>{{$user->forname.' '.$user->name}}</strong> <br>
                    {{$user->email}}<br>
                @else
                   <strong>{{$order->email}}</strong> <br>
                @endif
            {{$order->billing->address.', '.$order->billing->city.', '.$order->billing->country->name}}<br>
            P.O BOX: {{$order->billing->postal_code}}
            </p>

        </div>
    </div>
    <div style="width:100%;height: 100px;position: absolute;top: 50mm;left: 0">
        <h2 style="position: absolute;left: 0">Order's details</h2>
        <p style="color: grey;position: absolute;left: 0;top:40px">We thank you for your confidence. here is your bill</p>
        <div style="margin-top: 85px">
            <table  class="table table-bordered">
                <thead class="thead-inverse">
                <tr class="thead-inverse">
                    <th>Description</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->order_items as $oi)
                    <tr>
                        <td><strong>{{$oi->dosage->product->name}}- <span class="text-gray">{{$oi->dosage->description}}</span>   </strong></td>
                        <td><strong>{{$oi->price}} @lang('main.currency')</strong></td>
                        <td>{{$oi->quantity}}</td>
                        <td><strong>{{round($oi->price*$oi->quantity,2)}} @lang('main.currency')</strong></td>
                    </tr>

                @endforeach
                <tr>
                    <td colspan="3"><strong class="text-uppercase">SHIP TO {{$order->shipping->country->name}}</strong></td>
                    <td><strong>{{$order->shipping->country->ship_price}} @lang('main.currency')</strong></td>
                </tr>
                <tr>
                    <td colspan="3"><strong>TOTAL</strong></td>
                    <td><strong class="text-primary">{{$order->amount}} @lang('main.currency')</strong></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>

    <div style="width: 200px;height: 200px;position: absolute;top: 40%;left: 36%;">
        <img src="{{asset('images/paid.png')}}" style="width: inherit;height: inherit;opacity: 0.5"  >

    </div>

</div>