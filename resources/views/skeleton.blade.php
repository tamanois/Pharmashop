﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
	<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
	-->
	<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
	<script src="themes/js/less.js" type="text/javascript"></script> -->
	
<!-- Bootstrap style --> 
    <link id="callCss" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" media="screen"/>
<!-- Bootstrap style responsive -->
	<link href="{{asset('css/bootstrap-responsive.min.css')}}" rel="stylesheet"/>
	  <link href="{{asset('css/base.css')}}" rel="stylesheet" media="screen"/>

	  <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css">
<!-- Google-code-prettify -->	
	<link href="{{asset('js/google-code-prettify/prettify.css')}}" rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">


	  @yield('styles')
  </head>
<body style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">
<div class="loading_icon" style="
    position: fixed;
    z-index: 10000;
    Left: calc(50% - 75px);
    top: calc(50% - 75px);
    z-index: 10000;
     width: 150px;
     height: 150px;
         /*box-shadow: 0 10px 20px rgba(0, 0, 0, .5);*/
     display: none;
">
	<img src="{{URL::asset('images/loading.gif')}}" alt="" style="width: inherit;height: inherit">
</div>

<div class="loading-back" style="  position: fixed;
    z-index: 9999;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: none;
    background-color: rgba(255,255,255,0.7);"></div>
<?php

//class loading....................................
		if(!isset($cart)){
            $id_cart=\Illuminate\Support\Facades\Cookie::get('cart');
            if($id_cart){
                $cart=\pharmashop\DataCheck::getCart($id_cart);
                $camount=0;
                if($cart){
                    foreach ($cart->cart_products as $cp){
                        $camount+=($cp->dosage->price*$cp->quantity);
                    }
                    $camount=round($camount,2);

                    //session(['cart'=>$cart,'camount'=>$camount]);

                }


            }
		}

		if(!isset($home_policies)){
		    $home_policies=\pharmashop\Policy::orderBy('title')->get();
		}
		if(!isset($categories))
		    {
                $categories=\pharmashop\Category::with('products')->get();

            }


?>


<div id="header" style="margin-top: 0">
<div >
<div id="welcomeLine" class="container-fluid">
	<div class=" col-md-6 col-sm-6 ">@if(Auth::check()) <a href="{{route('account')}}" style="color: white !important;font-size: 15px"><span class="glyphicon glyphicon-user"></span> Welcome ! <strong> {{Auth::user()->forname.' '.Auth::user()->name}}</strong></a>@else
			<a href="{{route('login')}}" style="color: white;"><strong> <span class="glyphicon glyphicon-lock"></span> Login / Register</strong> </a> @endif </div>
	<div class="col-md-6 col-sm-6">
	<div class="pull-right">

		<span class="btn btn-warning btn-xs"><span id="upper-ccamount">{{isset($camount)?$camount:0}}</span> @lang('main.currency')</span>
 			<a href="{{route('show_cart')}}"><span style="color: white;font-size: 19px"><i class="glyphicon glyphicon-shopping-cart"></i> ( <span class="upper-ccount"> {{isset($cart)?$cart->cart_products->count():0 }} </span> ) </span> </a>
	</div>
	</div>
</div>
<!-- Navbar ================================================== -->

	<div id="menban" class="container-fluid" style="">

		<!-- Static navbar -->
		<nav class="navbar navbar-inverse" id="navbarnav" style="background: #8bc443;min-height: 112px ;margin-bottom: 10px;border:none;border-radius: 0">
			<div class="container-fluid container-fluid-nav">
				<div class="navbar-header" id="navbar-header">
					<button type="button" id="navbar-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img id="navbar-img" src="{{asset('images/logo.png')}}" style="width: 125px;height: 100px;"  alt=""></a>
				</div>
				<form class="navbar-form navbar-left" id="home_search_form" role="search" style="margin-top: 45px;border-color: white;border: none" method="GET" action="{{route('search_product')}}">
					<input type="hidden" name="type" value="name">
					<div class="form-group">
						<div class="input-group">
							<input id="home_search_form" name="value" class="form-control" placeholder="Search" style="background: rgba(255,255,255,0.4);">

							<div class="input-group-addon" onclick="document.getElementById('home_search_form').submit();">
								<span class="glyphicon gic-search alert-info"></span>
							</div>

						</div>
					</div>
				</form>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right" style="margin-top: 30px" >
						<li class="@if(isset($active_home)) active @endif"><a href="{{route('home')}}">HOME</a></li>

						<li class="dropdown" >
							<a href="#" class="dropdown-toggle text-uppercase @if(isset($active_products)) active @endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{route('search_product')}}"><strong class="text-uppercase">All products</strong></a></li>
								<li role="separator" class="divider"></li>
								<li class="dropdown-header"><strong class="text-uppercase">Categories</strong></li>
								@foreach($categories as $c)
									<li><a href="{{route('search_product').'?type=category&value='.$c->id}}" class="text-gray">{{$c->name}}</a></li>
								@endforeach

							</ul>
						</li>
						<li class="@if(isset($active_blog)) active @endif"><a href="{{route('blog_home')}}">BLOG</a></li>
						<li class="@if(isset($active_faq)) active @endif"><a href="{{route('faq_home')}}">FAQs</a></li>
						<li><a href="{{route('contact')}}" class="@if(isset($active_contact)) active @endif"><span style="display: inline" class="">CONTACT</span></a></li>

						@if(Auth::check())
							@if((Auth::user()->level)<2)
								<li><a href="{{route('dashboard')}}">DASHBOARD</a></li>
							@endif
							<li class="visible-xs"><a href="{{route('account')}}"><strong> My account </strong></a></li>
						@else
							<li class="visible-xs"><a href="{{route('login')}}">LOGIN / REGISTER</a></li>
						@endif
					<!--@if(Auth::check())<li><a href="{{route('logout')}}">Log out</a></li>@endif -->
					</ul>

				</div><!--/.nav-collapse -->
			</div><!--/.container-fluid -->
		</nav>

		<!-- Main component for a primary marketing message or call to action -->


	</div> <!-- /container -->



</div>
</div>

<div style="height: 50px">

</div>
<!-- Header End====================================================================== -->
@yield('home-caroussel')
<div id="mainBody">
	<div class="container body-container" style="padding-left: 0">
		@if(isset($alpha_searchable___))
			@include('alpha-search')
		@endif
	<div class="container-row row">
<!-- Sidebar ================================================== -->
	@yield('side-bar')
<!-- Sidebar end=============================================== -->




		<div class="container" style="min-height: 100px;">
			@yield('d-content')

		</div>

		</div>
	</div>
</div>


<div class="d2-content-container" style="min-height: 100px;">
	@yield('d2-content')

</div>
<!-- Footer ================================================================== -->
	<div  id="footerSection">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="col-md-3 col-sm-6">
					<h5><strong>ACCOUNT</strong></h5>
					<a href="{{route('account')}}">PERSONAL INFORMATION</a>
					<a href="{{route('account_addresses')}}">ADDRESSES</a>
					<a href="{{route('account_orders')}}">ORDER HISTORY</a>
				</div>
				<div class="col-md-3 col-sm-6">
					<h5><strong>INFORMATION</strong></h5>
					<a href="{{route('contact')}}">CONTACT</a>
					<a href="{{route('register')}}">REGISTRATION</a>
					<a href="{{route('faq_home')}}">FAQ</a>
				</div>
				<div class="col-md-3 col-sm-6">
					<h5><strong>ABOUT US</strong></h5>
					<a href="#">OUR COMPANY</a>
					<a href="{{route('account')}}">CONTACT</a>

				</div>
				<div class="col-md-3 col-sm-6">
					<h5><strong>POLICIES</strong></h5>
					@foreach($home_policies as $p)
						<a href="{{route('policy',$p->id)}}" class="text-uppercase">{{$p->title}}</a>
					@endforeach

				</div>
			</div>
			<div class="col-md-3">
				<div id="socialMedia" class="pull-right">
					<h5>SOCIAL MEDIA </h5>
					<a href="#"><img width="60" height="60" src="{{url('images/facebook.png')}}" title="facebook" alt="facebook"/></a>
					<a href="#"><img width="60" height="60" src="{{url('images/twitter.png')}}" title="twitter" alt="twitter"/></a>
					<a href="#"><img width="60" height="60" src="{{url('images/youtube.png')}}" title="youtube" alt="youtube"/></a>
				</div>
			</div>


		 </div>
		<!--<p class="pull-right">&copy; Bootshop</p>-->
	</div><!-- Container End -->
	</div>

<div class="modal fade" id="add_to_cart">
	<div class="modal-dialog special" >
		<div class="modal-content">
			<div class="modal-header" style="color: white;background: #286090">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Please confirm adding of the item</h4>
			</div>
			<div
					class="modal-body">



				<form accept-charset="UTF-8" role="form" id="confirm_cart_form" method="POST" action="#" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" id="ccart-id" name="id" required>

					<fieldset>
						<div class="row special">
							<div class="col-md-6" >


								<div class="form-group">
									<div id="targetOuter">
										<div id="confirm_img">
											<img src="{{asset('images/no_image.jpg')}}"  class="icon-choose-image" />
										</div>

									</div>


								</div>
								<div class="form-group">
									<h3 class="text-uppercase" id="ccart-name" style="font-weight: 600;font-size: 20px">Product name, Strength of product</h3>
									<h4 class="text-gray" id="ccart_gen_name">Generic-name</h4>
								</div>

							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="ccart_quantity" class="text-gray">Quantity</label>
									<select class="form-control" name="quantity" form="confirm_cart_form" id="ccart_quantity" onchange="ccart_change()">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="30">30</option>
										<option value="40">40</option>
										<option value="50">50</option>
										<option value="60">60</option>
										<option value="70">70</option>
										<option value="80">80</option>
										<option value="90">90</option>
										<option value="100">100</option>
									</select>
								</div>

								<div class="form-group">
									<h3 class="alert alert-info" style="font-weight: 500"> Amount: <span id="ccartt-amount"> 18.4 </span> @lang('main.currency')</h3>
								</div>
								<input type="hidden" id="ccart-price" name="price">

								<input type="hidden" id="ccart-amount" name="amount" required>
							</div>





						</div>





						<span class="btn btn-primary pull-right" onclick="confirm_cart_placement()"> <span class="glyphicon glyphicon-shopping-cart"></span> Add</span>

					</fieldset>

				</form>


			</div>





		</div>


	</div>
</div>

<a href="{{route('show_cart')}}" id="mobile-cart" class="visible-xs" style="height:90px;padding: 0 10px;position: fixed;z-index: 300;background: #fd8344; color: white;bottom: 150px;right: 0;border-radius: 4px">
	<div style="top: 40%;position: relative">
		<span style="font-weight:600;margin-top: 20px;margin-bottom: auto"><span class="glyphicon glyphicon-shopping-cart" style="font-weight:600;"></span> (<span class="upper-ccount">{{isset($cart)?$cart->cart_products->count():0 }}</span>)</span>

	</div>
</a>

<div id="js-alert" class="bg-primary" style="z-index: 10000000; width: 300px;position: fixed; left: -500px;top: -500px;border-radius: 3px;         box-shadow: 0 15px 25px rgba(0, 0, 0, .7);font-size: 18px;">
	<div class="text-center" style="padding: 10px 15px;margin: auto">
		<strong class="alert-text">Lorem ipsum dolor sit amet</strong>

	</div>

</div>
<div id="js-warning" class="bg-danger" style="z-index: 10000000; width: 300px;position: fixed; left: -500px;top: -500px;border-radius: 3px;         box-shadow: 0 15px 25px rgba(0, 0, 0, .7);font-size: 18px;">
	<div class="text-center text-danger" style="padding: 10px 15px;margin: auto">
		<strong class="alert-text">Lorem ipsum dolor sit amet</strong>

	</div>

</div>



<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/google-code-prettify/prettify.js')}}"></script>

	<script src="{{asset('js/bootshop.js')}}"></script>
    <script src="{{asset('js/jquery.lightbox-0.5.js')}}"></script>

<script>
	function showCcartModal(el)
	{
	    var e=$('#'+el)[0];

        var dosage=e.options[e.selectedIndex].id;
        // startLoading();
        $.ajax({
            url:'{{route('get_dosage')}}',
            type:'GET',
            data: 'id='+dosage,
            cache: false,
            contentType: false,
            processData: false,
            complete: function (res,stat) {
                var d=res.responseJSON;
                $('#ccart-id').val(d.id);
                $('#ccart_quantity').val(1);
                $('#confirm_img').html('<img src="'+ d.product.imgurl +'" />');
                $('#ccart-name').html(d.product.name+' - '+d.description);
                $('#ccart_gen_name').html(d.product.generic_name);
                $('#ccart-price').val(d.price);
                $('#ccart-amount').val(d.price);
                $('#ccartt-amount').html(d.price);

                $('#add_to_cart').modal('show');
            }
        })

    }
    function ccart_change()
	{
	    var v=$('#ccart_quantity').val();
	    var p=$('#ccart-price').val();
	    var t=v*p;
	    $('#ccart-amount').val(t.toFixed(2));
        $('#ccartt-amount').html(t.toFixed(2))


    }

    function confirm_cart_placement()
    {
        startLoading();
        $.ajax({
            url:'{{route('update_cart')}}',
            type:'POST',
            data: new FormData($('#confirm_cart_form')[0]),
            cache: false,
            contentType: false,
            processData: false,
            complete: function (res,stat) {
				//$('body').html(res.responseText);
                stopLoading();
                $('#add_to_cart').modal('hide');
                var data=res.responseJSON[0];
				$('.upper-ccount').each(function (i,el) {
                    console.log(el);
					el.innerHTML=data.ccount;

                });
                $('#lower-ccount').html(data.ccount);
                $('#upper-ccamount').html(data.camount);
                $('#lower-ccamount').html(data.camount);


            }
        })

    }


    function refresh_cart()
    {
        $.ajax({
            url:'{{route('refresh_cart')}}',
            type:'GET',
            data: '',
            cache: false,
            contentType: false,
            processData: false,
            complete: function (res,stat) {
                var data=res.responseJSON[0];
                $('.upper-ccount').each(function (i,el) {
                    console.log(el);
                    el.innerHTML=data.ccount;
                });
                $('#lower-ccount').html(data.ccount);
                $('#upper-ccamount').html(data.camount);
                $('#lower-ccamount').html(data.camount);

            }
        })

    }

    function startLoading() {
        $('.loading-back').css('display','inline');
        $('.loading_icon').css('display','inline');
    }
    function stopLoading() {
        $('.loading-back').css('display','none');
        $('.loading_icon').css('display','none');
    }
    function showAlert(text,type){
	    var el;
	    if(type===0){
	        el=$('#js-alert');
		}else if(type===1)
		{
            el=$('#js-warning');
        }
        var textEl=el.find('.alert-text');
	    textEl.html(text);
	    el.css('left','calc(50% - 150px)');
        el.css('top','45%');
        el.css('opacity',1);

	    setTimeout(function () {
            el.css('opacity',0);

        },3000);
        setTimeout(function () {
            el.css('left','-500px');
            el.css('top','-500px');
        },1500);


	}

    function getToken() {
        var token=false;
        $.ajax({
            type: 'GET',
            url: '{{route('get_token')}}',
            cache: false,
			async: false,
            timeout: 30000,
            success: function (data) {
                //console.log(data.token);
				token =data.token;
            },
            error: function (data) {
                showAlert('Error getting token',1);
                token= false;
            }
        });
	return token;
    }
</script>
	@yield('scripts')

	
	<!-- Themes switcher section ============================================================================================= -->

</body>
</html>