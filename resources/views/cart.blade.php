﻿@extends('sidebar-skeleton')


@section('content')

	@if($cart_products->count()>0)
		<div class="col-md-12">

			<a style="margin-bottom: 10px" href="{{route('search_product')}}" class="btn btn-default btn-lg pull-right"><i class="glyphicon glyphicon-arrow-left"></i> Continue Shopping </a>
			<form action="{{route('confirm_cart')}}" method="post" id="cart-form">
				{{csrf_field()}}
				<table style="font-size: 13px" class="table  table-condensed  table-striped table-responsive">
					<thead>
					<tr>
						<th>Product</th>
						<th>Description</th>
						<th>Quantity/Update</th>
						<th>Price</th>
						<th>Total</th>
					</tr>
					</thead>
					<tbody>
                    <?php $total=0;$i=0 ?>
					@foreach($cart_products as $cp)
						<tr id="cart-row{{$i}}">
							<input type="hidden" name="idCartProducts[]" value="{{$cp->id}}">
							<td> <img width="60" src="{{asset($cp->dosage->product->imgurl)}}" alt=""/></td>
							<td><strong>{{$cp->dosage->product->name.' - '.$cp->dosage->description}}</strong><br/>{{$cp->dosage->product->generic_name}}</td>
							<td>
								<div class="btn-group" >
									<input type="hidden" name="quantities[]" id="cpQuantities{{$i}}" value="{{$cp->quantity}}" >
									<button style="padding-top: 3px;padding-bottom: 3px" class="btn btn-default" type="button" id="col-quantity{{$i}}">{{$cp->quantity}}</button>
									<button class="btn btn-default" type="button" onclick="quantityChange('col-quantity{{$i}}',0,'col-price{{$i}}','col-mini-total{{$i}}','cpQuantities{{$i}}')"><i class="glyphicon glyphicon-minus"></i></button>
									<button class="btn btn-default" type="button" onclick="quantityChange('col-quantity{{$i}}',1,'col-price{{$i}}','col-mini-total{{$i}}','cpQuantities{{$i}}')"><i class="glyphicon glyphicon-plus"></i></button>
									<button class="btn btn-danger" type="button" onclick="deleteCartProduct('cart-row{{$i}}',{{$cp->id}})"><i class="glyphicon glyphicon-remove"></i></button>
								</div>
							</td>
							<td><strong><span id="col-price{{$i}}" class="col-price">{{$cp->dosage->price}}</span> @lang('main.currency')</strong></td>
                            <?php $agg=$cp->dosage->price*$cp->quantity;$total+=$agg ?>
							<td><strong class="text-primary"><span id="col-mini-total{{$i}}" class="col-mini-total">{{round($agg,2)}}</span> @lang('main.currency')</strong></td>

						</tr>
                        <?php $i++ ?>
					@endforeach



					<tr>
						<td colspan="4" style="text-align:right">Total Price:	</td>
						<td><strong><span id="total-price">{{round($total,2)}}</span> @lang('main.currency')</strong> </td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:left;font-size: 15px"><a href="#choose_ship_country" data-toggle="modal" data-backdrop="false"> <strong style="text-decoration: underline" id="ship_text">@if(isset($shipCountry)) {{$shipCountry->name}} @else Choose shipping country @endif </strong></a>	</td>
						<td colspan="2" style="text-align:right"> <strong >Shipping price:</strong>	</td>
						<td ><strong><span id="ship-price">@if(isset($shipCountry)) {{$shipCountry->ship_price}} @else 0 @endif</span> @lang('main.currency')</strong> </td>
						<input type="hidden" name="ship_country" id="shippingCountry" @if(isset($shipCountry)) value="{{$shipCountry->id}}" @endif>
					</tr>

					<tr>
						<td colspan="4" style="text-align:right"><strong>TOTAL =</strong></td>

						<td class="btn btn-danger" style="display:block"> <strong> <span id="total-expense"> @if(isset($shipCountry)) {{round($shipCountry->ship_price+$total,2)}} @else {{round($total,2)}} @endif</span> @lang('main.currency')</strong></td>
					</tr>
					</tbody>
				</table>

			</form>

		</div>




<div class="row-fluid">
	<div class="col-md-6">
		<a href="{{route('search_product')}}" class="btn btn-default btn-lg"><i class="glyphicon glyphicon-arrow-left"></i> Continue Shopping </a>

	</div>
	<div class="col-md-1" style="margin-top: 15px"></div>
	<div class="col-md-5">
		<div class="btn btn-success btn-lg pull-right" onclick="submitCart()">Checkout <i class="glyphicon glyphicon-arrow-right"></i></div>

	</div>
</div>





	@else
	<div class="col-md-12">
		<h3 class="alert alert-info">Cart is empty</h3>
		<a href="{{route('search_product')}}" class="btn btn-default btn-lg"><i class="glyphicon glyphicon-arrow-left"></i> Continue Shopping </a>

	</div>
	@endif
	<div id="empty-cart-code" style="display: none">
		<div class="col-md-12">
			<h3 class="alert alert-info">Cart is empty</h3>
			<a href="{{route('search_product')}}" class="btn btn-default btn-lg"><i class="glyphicon glyphicon-arrow-left"></i> Continue Shopping </a>

		</div>
	</div>
<div class="modal fade" id="choose_ship_country">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Select a country</h4>
			</div>
			<div class="modal-body" style="height: 150px">
					<label >Country</label>
					<div class="form-group">
						<select name="country" class="form-control" id="ship_country" style="padding-top: 15px; padding-bottom: 15px;height: 50px;">
							<option value="0">-- Choose shipping country --</option>
							@foreach($countries as $c)
								@if($c->ship==1)
									<option id="sp{{$c->id}}" value="{{$c->ship_price}}">{{$c->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="btn btn-primary pull-right" data-dismiss="modal" onclick="setShipPrice();"><span class="glyphicon glyphicon-ok-sign"></span> OK</div>



			</div>

		</div>


	</div>
</div>
@endsection

@section('styles')
	<style>
		#header
		{
			margin-top: -40px !important;
		}
	</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>
		function quantityChange(elId,op,elPriceId,elMinTotalId,elQuantityId) {
		    var jqEl=$('#'+elId);
		    var jqPrice=$('#'+elPriceId);
            var jqMinTotal=$('#'+elMinTotalId);
            var jqQuantityId=$('#'+elQuantityId);

            var p=parseFloat(jqPrice.html());


            var n=parseInt(jqEl.html());
		    if(op===0)
			{
			    if(n>1)
				{
				    n=n-1;
				}
			}else if (op===1)
			{
                n=n+1;
			}
			var t=p*n;
			jqEl.html(n);
			jqQuantityId.val(n);
			jqMinTotal.html(t.toFixed(2));
			evaluateTotal();

        }
        function evaluateTotal() {
			var total=0;
            var p;
			$('.col-mini-total').each(function () {
                p=parseFloat(this.innerHTML);
                total=total+p;
            });
			p=parseFloat($('#ship-price').html());
            $('#total-price').html(total.toFixed(2));

            total=total+p;


            $('#total-expense').html(total.toFixed(2));
        }
        function setShipPrice() {
		    var v=$('#ship_country').val();
		    var el=$('#ship_country')[0];
		    var elInput=$('#shippingCountry');
		    var name=el.options[el.selectedIndex].innerHTML;
            var id=el.options[el.selectedIndex].id;
            id= id.substring(2);
            id= parseInt(id);
            elInput.val(id);
            $('#ship-price').html(v);
		    $('#ship_text').html(name);

		    evaluateTotal();
        }

        function deleteCartProduct(rowEl,id) {
            startLoading();
            $.ajax({
                url:'{{route('remove_cart_product')}}',
                type:'GET',
                data: 'id='+id,
                cache: false,
                contentType: false,
                processData: false,
                complete: function (res,stat) {
                    console.log(res.responseText);
                    if(res.responseText==='OK'){
                        $('#'+rowEl).remove();
                        stopLoading();
                        evaluateTotal();
                        refresh_cart();
					}
                    else if(res.responseText=='0'){
                        var html=$('#empty-cart-code').html();
                        $('#aside-content').html(html);
                        refresh_cart();
                        stopLoading();
                    }
					else {
                        stopLoading();
					}

                }
            })
        }

        function submitCart(){
			$('#cart-form').submit();
		}

	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection