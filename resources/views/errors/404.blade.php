@extends('skeleton')
@section('title')
	404 not found
@endsection
@section('d-content')

	<style>


		.container.c404 {
			text-align: center;
			display: table-cell;
			vertical-align: middle;
		}

		.content {
			color: #a94442;
			font-family: 'Arial Narrow';
			text-align: center;
			display: inline-block;
		}

		.title {
			font-size: 72px;
			margin-bottom: 40px;
		}
	</style>
	<div class="container c404">
		<div class="content">
			<div class="title">404 Not Found.</div>
			<p>Sorry the page you requested was not found</p>
		</div>
	</div>

@endsection

