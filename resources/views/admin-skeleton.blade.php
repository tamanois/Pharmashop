<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{isset($title)?$title:'Administrator'}}</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">


    <style type="text/css">
        .module1{

            border-radius: 4px;
            padding-left: 5px;
            padding-right: 5px;


        }

        .module2{
            margin-top: 25px;
            border-radius: 2px;
            padding-left: 2px;
            padding-right: 2px;
            height: 100px;
        }
        .titre4,h5{
            color: #fefaf9;
        }
    </style>
    <style>
        #targetOuter00 img
        {
            height: 150px;
            width: auto;
        }

    </style>
    @yield('styles')
</head>
<?php
    //load data
        $not_count=\pharmashop\Notification::where('dismissed','=',0)->get()->count();
        $o=\pharmashop\Order::where('status','=',5)->where('checked','=',0)->count();
        if($o>0)
            $not_count++;

?>
<div class="loading-back" style="  position: fixed;
    z-index: 9999;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: none;
    background-color: rgba(255,255,255,0.7);"></div>
<body>
<div class="loading_icon" style="
    position: fixed;
    Left: calc(50% - 75px);
    top: calc(50% - 75px);
    z-index: 10000;
     width: 150px;
     height: 150px;
     display: none;
">


    <img src="{{URL::asset('images/loading.gif')}}" alt="" style="width: inherit;height: inherit">
</div>
<div class="nav-side-menu">
    <div class="brand">Welcome <strong> {{Auth::user()->forname.' '.Auth::user()->name}}</strong></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li class="active">
                <a href="{{route('dashboard')}}" class="menu-item">
                    <i class="fa fa-dashboard fa-lg"></i> Dashboard
                </a>
            </li>

            <li onclick='window.location.href="{{route('home')}}"'>
                <a href="{{route('home')}}" class="menu-item">
                    <i class="fa fa-home fa-lg"></i> Home
                </a>
            </li>

            <li  data-toggle="collapse" data-target="#products" class="collapsed">
                <a href="#" class="menu-item"><i class="fa fa-edit fa-lg"></i> Basic configuration <span class="arrow"></span></a>
            </li>
            <ul class="sub-menu collapse" id="products" >
                <li onclick='window.location.href="{{route('category_management')}}"'><a href="{{route('category_management')}}">Manage categories</a></li>
                <li onclick='window.location.href="{{route('product_management')}}"'><a href="{{route('product_management')}}">Manage Products</a></li>

                <li onclick='window.location.href="{{route('country_management')}}"'><a href="{{route('country_management')}}">Manage countries</a></li>
                <li onclick='window.location.href="{{route('faq_category_management')}}"'><a href="{{route('faq_category_management')}}">Manage Faq's categories</a></li>
                <li onclick='window.location.href="{{route('faq_management')}}"'><a href="{{route('faq_management')}}">Manage FAQs</a></li>
                <li onclick='window.location.href="{{route('policy_management')}}"'><a href="{{route('policy_management')}}">Manage Policies</a></li>
                <li onclick='window.location.href="{{route('article_management')}}"'><a href="{{route('article_management')}}">Manage Articles</a></li>

            </ul>

            <li onclick='window.location.href="{{route('site_config')}}"'>
                <a href="{{route('site_config')}}" class="menu-item">
                    <i class="fa fa-globe fa-lg"></i> Site configuration
                </a>
            </li>
            <li onclick='window.location.href="{{route('user_management')}}"'>
                <a href="{{route('user_management')}}" class="menu-item">
                    <i class="fa fa-user fa-lg"></i> Users
                </a>
            </li>
            <li onclick='window.location.href="{{route('order_management')}}"'>
                <a href="{{route('order_management')}}" class="menu-item">
                    <i class="fa fa-shopping-cart fa-lg"></i> Orders
                </a>
            </li>





            <li onclick='window.location.href="{{route('logout')}}"'>
                <a href="{{route('logout')}}" class="menu-item" style="color: #1391e8;">
                    <i class="fa fa-user fa-lg"></i> Logout
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="container-fluid page" style="padding-right: 0">
    <div class="big-container col-md-12" style="padding-left: 0;padding-right: 0">
        <div class="upper-band">
            <div class="content pull-right">
                <a href="{{route('notification_management')}}" class="upper-band-el"><span class="glyphicon glyphicon-globe"></span> Notifications <i class="badge red">{{$not_count}}</i></a>
               
               <!-- <a href="#" class="upper-band-el"><span class="fa fa-table"></span> Settings</a> -->

                <p class="conn-as">
                    Login as <i class="blue">Administrator</i>
                </p>




            </div>




        </div>
        <div class="band-gray">

        </div>
        <div class="body-head">
                <div class="home body-head-el">
                    <span><span class="fa fa-dashboard" style="font-size: 28px"></span> {{isset($big_title)?$big_title:''}}</span>
                </div>
                <div class="search body-head-el col-md-6 pull-right">
                    <form action="#">
                        <div class="col-md-8 col-sm-7 col-xs-6">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                                <input type="text" class="form-control">
                            </div>

                        </div>
                        <div class="col-md-4 col-sm-5 col-sm-6">
                            <input type="submit" class="btn btn btn-primary" value="rechercher">

                        </div>
                    </form>
                </div>
        </div>

        <div class="container myContainer ">
            @if(isset($isDashboard))
                <div style="margin-bottom: 10px"></div>
                @else
            <h3>{{isset($title)?$title:''}}</h3>
                <hr>

            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
            @if(session('success'))
                <div class="alert alert-success">
                    <strong>Successful</strong>
                </div>
            @endif

            @yield('content')



        </div>


        <div class="footer">
            <p class="text-center" style="color: white;font-weight: 500;"><span class="glyphicon glyphicon-copyright-mark"></span> Copyright 2018</p>
        </div>

    </div>

</div>

</body>

<div class="modal fade" id="consulter-note-modal">
    <div class="modal-dialog" style="width: 30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title text-center text-primary" style="font-weight: 600">
                   Consulter des Notes
                </h4>
            </div>
            <div
                    class="modal-body">




            </div>





        </div>


    </div>
</div>


<div class="modal fade" id="image-modal">
    <div class="modal-dialog special ">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h4 class="modal-title col-md-6 col-sm-6">Image gallery</h4>
                        <div class="col-md-6 col-sm-6">
                            <a href="#upload-image-modal" data-toggle="modal" class="btn btn-success pull-right">
                                <span class="glyphicon glyphicon-upload"> </span>
                                Upload image
                            </a>
                        </div>

                    </div>
                </div>

            </div>
            <div
                    class="modal-body">
                <div class="row">
                    <div class="container-fluid">
                        <div id="image-container" class="col-md-12">

                        </div>
                    </div>


                </div>


            </div>


        </div>
    </div>
</div>

<div class="modal fade" id="upload-image-modal">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Upload image</h4>
                    </div>

            <div class="modal-body">




                <form accept-charset="UTF-8" role="form" id="image-gallery-form" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}



                        <div id="gallery-msg" class="alert alert-danger" style="display: none">


                        </div>
                    <div id="gallery-msg-success" class="alert alert-success" style="display: none">

                        <p>The image has been uploaded</p>
                    </div>
                            <div class="" >
                                <div id="targetOuter00">
                                    <div class="" id="targetLayer00">

                                        <img style="" src="{{asset('images/no_image.jpg')}}"  class="icon-choose-image" />
                                    </div>
                                    <div class="">
                                        <input name="img" id=""  type="file" class="form-control" onChange="showPreviewGallery(this,'targetLayer00');" />
                                    </div>
                                    <div>
                                        <div class="btn btn-success" onclick="storeImage()"><span class="glyphicon glyphicon-plus"> </span> Add image</div>
                                    </div>
                                </div>


                            </div>






                </form>


            </div>


        </div>
    </div>

</div>



<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>

@yield('scripts')
<script>
    function master_check_change(element) {
        if(element.checked){
            $('.check-list').each(function (e,el) {
                el.checked=true;
            })
        }
        else {
            $('.check-list').each(function (e,el) {
                el.checked=false;
            })
        }

    }

    function sendToDelete(url) {
        var data={},checklist={};
        data['_token']='{{ csrf_token() }}';
        var i=0;
        $('.check-list').each(function (e,el) {
            if(el.checked){
                checklist[i]=el.value;
                i++;

            }


        });
        if(i>0){
            data['check']=checklist;
            postDelete(JSON.stringify(data),url)
        }





    }
    function postDelete(data,url) {
        $.ajax({
            url:url,
            type:'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: data,
            cache: false,
            complete: function (res,stat) {
                location.reload();
            }
        })

    }

    function getContent(url,id) {
        $.ajax({
            url:url,
            type:'POST',
            dataType: 'POST',
            data: "_token={{csrf_token()}}&id="+id,
            cache: false,
            complete: function (res,stat) {
                CKEDITOR.instances.edit_content.setData(res.responseText);

            }
        })

    }
    function startLoading() {
        $('.loading-back').css('display','inline');

        $('.loading_icon').css('display','inline');
    }
    function stopLoading() {
        $('.loading-back').css('display','none');
        $('.loading_icon').css('display','none');
    }
</script>
<script>
    function showPreview(objFileInput,elementName) {
        if (objFileInput.files[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $("#"+elementName).html('<img  src="'+e.target.result+'" style="width: 100%" class="icon-choose-image" />');
                $("#"+elementName).css('opacity','0.8');
            };
            startLoading();
            fileReader.readAsDataURL(objFileInput.files[0]);
            stopLoading();
        }
    }
    function showPreviewGallery(objFileInput,elementName) {
        if (objFileInput.files[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $("#"+elementName).html('<img  src="'+e.target.result+'"  class="icon-choose-image" />');
                $("#"+elementName).css('opacity','0.8');
            };
            startLoading();
            fileReader.readAsDataURL(objFileInput.files[0]);
            stopLoading();
        }
    }

    function storeImage() {
        var galms=$('#gallery-msg');
        var galmsuc=$('#gallery-msg-success');
        galmsuc.css('display','none');

        galms.empty();
        galms.css('display','none');

        var ic=$('#image-container');
        startLoading();
        $.ajax({
            url: "{{route('store_image')}}",
            type: "POST",
            data:  new FormData($("#image-gallery-form")[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(data)
            {


                stopLoading();
                galmsuc.css('display','block');

                console.log(data);

                fillImage(data,ic);


            },
            error: function(data)
            {
                stopLoading();

                var i;
                var d=data.responseJSON;
                galms.css('display','block');
                galms.html('<p><strong>There are some errors:</strong></p> <ul class="alert alert-danger"> </ul>');
                galms=galms.find('ul');
                for(i=0;i<d.length;i++){
                    galms.append('<li>'+ d[i] +'</li>')
                }
            }
        });
    }

    function getImage() {
        var galms=$('#gallery-msg');
        var ic=$('#image-container');
        galms.empty();
        startLoading();
        $.ajax({
            url: "{{route('image_management')}}",
            type: "GET",
            cache: false,

            success: function(data)
            {

                console.log(data);

                fillImage(data,ic);


            },
            error: function(data)
            {
                stopLoading();
                console.log(data);

                galms.html('<ul class="alert alert-danger"> <strong>There are some errors:</strong></ul>');
                galms=galms.find('ul');

            }
        });
    }

    function delImage(id) {
        var galms=$('#gallery-msg');
        var ic=$('#image-container');
        galms.empty();
        startLoading();
        $.ajax({
            url: "{{route('delete_image')}}",
            type: "GET",
            cache: false,
            data: 'id='+id,
            success: function(data)
            {

                console.log(data);
                fillImage(data,ic);

            },
            error: function(data)
            {
                stopLoading();
                console.log(data);

                galms.html('<ul class="alert alert-danger"> <strong>There are some errors:</strong></ul>');
                galms=galms.find('ul');

            }
        });
    }

    function fillImage(data,ic) {
        var i;
        ic.empty();

        var d=data;

        for(i=0;i<d.length;i++){

            ic.append('<div class="col-md-2 col-sm-3 col-xs-4" style="padding: 4px 8px"> <div class="image-item"> <img src="' + d[i].thumbUrl + '" alt=""> </div> ' +
                '<input id="imgurl'+ i +'" type="text" class="form-control" value="'+ d[i].url +'" disabled> ' +
                '<div class="btn-group "><div class="btn btn-info " onclick="copyToClipboard(\'imgurl'+ i +'\')"><span class="glyphicon glyphicon-copy"></span> COPY URL</div> <div class="btn btn-danger" onclick="delImage('+ d[i].id +')"><span class="glyphicon glyphicon-trash"></span></div></div> </div>');
        }
        if(parseInt(d.length)===0)
            ic.append('<span class="text-gray" style="font-size: 18px"> </span>');
        stopLoading();
    }

    function copyToClipboard(elementId) {

        // Create a "hidden" input
        var aux = document.createElement("input");

        // Assign it the value of the specified element
        aux.setAttribute("value", document.getElementById(elementId).value);

        // Append it to the body
        document.body.appendChild(aux);

        // Highlight its content
        aux.select();

        // Copy the highlighted text
        document.execCommand("copy");

        // Remove it from the body
        document.body.removeChild(aux);
        alert('URL copied')

    }
</script>
</html>