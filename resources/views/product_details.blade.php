﻿@extends('sidebar-skeleton')


@section('content')
	<div >
		<div class="">

			<div class="row">
				<div id="gallery" class="col-md-3">
					<a href="#" title="">
						<img src="{{asset($product->imgurl)}}" style="width: 175px" alt=""/>
					</a>



				</div>
				<div class="col-md-9">
					<h3 style="font-weight: bold" class="text-uppercase">{{$product->name}}  </h3>
					<small>{{$product->generic_name}}</small>
					<hr class="soft"/>
					<div class="row">
							<div class="col-md-3">
								<h2 ><span id="price_p">{{($price=$product->dosages->first())?$price->price:0}}</span> @lang('main.currency')</h2>

							</div>

						<div class="col-md-4 col-sm-6">
							<label for="">Strength:</label>
							<select name="" id="dosage_p" class="form-control" onchange="dosageChange(this,'price_p')">
								@foreach($product->dosages as $d)
									<option id="{{$d->id}}" value="{{$d->price}}">{{$d->description}}</option>
								@endforeach
							</select>
						</div>

						<button style="margin-top: 20px" type="submit" class="btn btn-lg btn-primary pull-right" onclick="showCcartModal('dosage_p')"> Add to cart <i class="glyphicon glyphicon-shopping-cart 		"></i></button>

					</div>



					<hr class="soft"/>


				</div>

			</div>
			<div class="container-fluid">
				<h3>Medical description</h3>
				<hr>
				 @if($product->description!=null)
				<?php echo($product->description) ?>
					 @else
					<h4 style="color: grey">No description has been provided</h4>
				@endif
			</div>
		</div>
	</div>


@endsection

@section('styles')
	<style>
		#header
		{
			margin-top: -40px !important;
		}
	</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>
        function dosageChange(el,priceEl) {
            var v=el.value;
            $('#'+priceEl).html(v);
        }


	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection