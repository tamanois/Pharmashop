﻿@extends('skeleton')
@section('home-caroussel')
	@yield('home-caroussel')
@endsection

<?php
//class loading....................................
$categories=\pharmashop\Category::with('products')->get();
$id_cart=\Illuminate\Support\Facades\Cookie::get('cart');
if($id_cart){
    $cart=\pharmashop\DataCheck::getCart($id_cart);
    $camount=0;
    if($cart){
        foreach ($cart->cart_products as $cp){
            $camount+=($cp->dosage->price*$cp->quantity);
        }
        $camount=round($camount,2);

        //session(['cart'=>$cart,'camount'=>$camount]);

    }


}

?>
<div class="row">

	@section('side-bar')
		<div id="sidebar" class="col-md-3 col-sm-4 hidden-xs" style="padding-left: 35px">
			<?php 			/*<div class="well well-small"><a id="myCart" href="{{route('show_cart')}}"><img src="{{asset('images/ico-cart.png')}}" alt="cart"><span id="lower-ccount">{{isset($cart)?$cart->cart_products->count():0 }}</span> cart's items  <span class="badge badge-warning"><span id="lower-ccamount">{{isset($camount)?$camount:0}}</span> @lang('main.currency')</span></a></div> */ ?>
			<div id="sidemenu-title"  style="background: #8bc443;margin-bottom: 2px ;padding:5px 10px; border-radius: 4px 4px 0 0;"><h4 style="font-size: 24px;color: white;">Categories</h4></div>

			<ul id="sideManu" class="nav nav-tabs nav-stacked">
			@foreach($categories as $c)
					<li><a href="{{route('search_product').'?type=category&value='.$c->id}}" class="text-gray">{{$c->name}} [{{$c->products->count()}}]</a></li>
				@endforeach

			</ul>
			<br/>

			<div class="thumbnail">
				<img src="{{asset('images/payment_methods.png')}}" title="Bootshop Payment Methods" alt="Payments Methods">
				<div class="caption">
					<h5>Payment Methods</h5>
				</div>
			</div>
		</div>

		<div class="col-md-9 col-sm-8">
			@if(!isset($active_home_page))
			<h3 style="margin-bottom: 0;color: #78a648"> @if(isset($page_title)){{$page_title}}@endif</h3>
			<hr class="soft"/>
			@endif
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
			@if(count($errors)>0)
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>

			@endif
<div id="aside-content">
	@yield('content')

</div>
		</div>
	@endsection

</div>








@section('styles')
	<style>
		#header
		{
			margin-top: -20px !important;
		}
	</style>
	@yield('styles')
@endsection

@section('scripts')
	@yield('scripts')
@endsection