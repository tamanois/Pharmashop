﻿@extends('skeleton')


@section('d-content')
<div class="text-center">
	<h2 class=""><strong>BLOG</strong></h2>
</div>
	<div class="post-container">
        @foreach($articles as $a)
            <div class="post">
                <div class="post-body">
                    <div class="post-icon">
                        <img src="{{asset('images/blog-icon.png')}}" alt="">
                    </div>
                    <div class="post-info">
                        <div class="post-title">
                            <h4 >{{$a->title}}</h4>
                        </div>
                        <div class="post-detail text-uppercase">
                            <span class=""><span class="glyphicon glyphicon-calendar"></span> {{(new DateTime($a->created_at))->format('l, F j, Y')}}</span>
                            <span class="detail"><span class="glyphicon glyphicon-user"></span> Posted by {{$a->author->forname.' '.$a->author->name}}</span>

                        </div>
                        <div class="post-divbar">
                            <div class="divbar1"></div>
                            <div class="divbar2"></div>
                            <div class="divbar3"></div>
                        </div>
                        <div class="post-text">
                                {{$a->summary}}

                        </div>
                    </div>
                </div>
                <div class="post-footer">
                    <a href="{{route('show_article',$a->id)}}" class="btn btn-primary" style="margin-top: 20px;margin-left: 30px;font-weight: 600;"> Read more >></a>
                </div>


            </div>
            <hr>
        @endforeach

	</div>

@endsection

@section('styles')
		<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
		</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>



	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection