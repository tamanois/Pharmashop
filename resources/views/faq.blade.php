﻿@extends('skeleton')


@section('d-content')
<div class="">
	<h2 class="alert "><strong>HELP & FAQ</strong></h2>
</div>
	<div class="faq-container">
        <div class="category col-md-3 col-sm-3">
            @foreach($faq_categories as $fq)
                <div>
                    <h4 class=""><a href="{{route('faq_home').'?category='.$fq->id}}">{{$fq->name}}</a></h4>
                    <hr>
                </div>
            @endforeach

        </div>
        <div class="question col-md-9 col-sm-9">
            <h3>{{isset($cat)?$cat->name:''}}</h3>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php $i=0 ?>
                @foreach($faqs as $f)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne{{$i}}">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{$i}}" aria-expanded="true" aria-controls="collapseOne" class="">
                                    <h5 class="question"> {{$f->title}}</h5>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne{{$i}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <?php echo ($f->content)?>
                            </div>
                        </div>
                    </div>
                    <?php $i++ ?>
                @endforeach


            </div>
        </div>





	</div>

@endsection

@section('styles')
		<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
		</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>



	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection