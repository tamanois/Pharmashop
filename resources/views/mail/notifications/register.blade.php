@component('mail::message')
<h2>Register notification</h2>

<p> Hello {{$name}}, You successfully register to @lang('main.site_name') services, You can now follow your order </p>



Thanks,<br>
{{ config('app.name') }}
@endcomponent
