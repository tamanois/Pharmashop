@component('mail::message')
<h1>Order Notification</h1>

@component('mail::panel')
    <p>Hello, You are order was successful paid ! <br>
        The bill is given as attachment .Your delivery will be done soon.
        Thanks.
    </p>
@endcomponent

{{ config('app.name') }}
@endcomponent
