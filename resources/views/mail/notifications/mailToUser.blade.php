@component('mail::message')
<h1>Contact mail</h1>
<p>Hello. <strong>{{$sender['name']}}</strong> as written a message via the contact page of the platform </p>
<p>Here it is:</p>
@component('mail::panel')
    <p>{{$sender['message']}}</p>
@endcomponent

<p>Please give a quick answer at <strong>{{$sender['email']}}</strong> </p>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
