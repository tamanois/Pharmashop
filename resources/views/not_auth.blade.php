@extends('skeleton')
@section('title')
	Access denied
@endsection
@section('d-content')

	<style>


		.container.c404 {
			text-align: center;
			display: table-cell;
			vertical-align: middle;
		}

		.content {
			color: #a94442;
			font-family: 'Arial Narrow';
			text-align: center;
			display: inline-block;
		}

		.title {
			font-size: 72px;
			margin-bottom: 40px;
		}
	</style>
	<div class="container c404">
		<div class="content">
			<div class="title">Access denied.</div>
			<p>Sorry you are not authorized to access this page</p>
		</div>
	</div>

@endsection

