@extends('admin-skeleton')
@section('content')
    @include('perso.functions')

    <div class="col-md-12">

        <button class="btn btn-primary" data-toggle="modal" data-backdrop="false" href="#add_cat"
        ><span class="glyphicon glyphicon-plus"></span> New Product</button>
        <td><a href="#multi-delete" id="multi-delete" onclick="sendToDelete('{{route('mdelete_product')}}')"  class="btn btn-danger btn-xs pull-right"><span class="glyphicon glyphicon-trash"></span> remove</a></td>

        <table class="table  table-striped table-condensed table-inverse" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>#</th>
                <th style="min-width: 100px">Name</th>
                <th>Generic name</th>
                <th>Image</th>
                <th>Category</th>
                <th></th>
                <th></th>
                <th><input type="checkbox" id="master-check" onchange="master_check_change(this)"> </th>
            </tr>

            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($products as $f)
                <tr >
                    <td>{{$i++}}</td>
                    <td><a href="#edit_cat" data-toggle="modal" data-backdrop="false" onclick="edit_product({{$f->id}},'{{sanitize($f->name)}}','{{sanitize($f->generic_name)}}','{{($f->imgurl)?asset($f->imgurl):''}}','{{$f->id_category}}')"><strong ><span class="glyphicon glyphicon-edit"></span> {{$f->name}}</strong></a></td>
                    <td> {{$f->generic_name}} </td>
                    <td>
                        {{($f->imgurl)?'Yes':'No'}}
                    </td>
                    <td>{{$f->category->name}}</td>
                    <th><a href="{{route('dosage_management',$f->id)}}" class="btn btn-warning btn-xs"><span class="glyphicon  glyphicon-grain"></span> Strength</a></th>
                    <td>@if($f->feature==0) <span id="fc{{$f->id}}"><a href="#featEl{{$f->id}}" id="featEl{{$f->id}}" onclick="updateFeature({{$f->id}},1)" class="btn btn-success btn-xs"> Set as featured</a> @else <span id="fc{{$f->id}}"> <a href="#featEl{{$f->id}}" id="featEl{{$f->id}}" onclick="updateFeature({{$f->id}},0)" class="btn btn-danger btn-xs"> Remove from featured</a></span>   @endif</span> </td>
                    <td><input type="checkbox" name="delete[]" value="{{$f->id}}" class="check-list" ></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="add_cat">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" enctype="multipart/form-data" role="form" id="product-form" method="POST" action="{{route('store_product')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>

                                <div >


                                    <div class="form-group col-md-6">
                                        <div id="targetOuter">
                                            <div id="targetLayer0">
                                                        <img src="{{asset('images/no_image.jpg')}}"  class="icon-choose-image" />
                                            </div>
                                            <div  >
                                                <input name="img" id="image"  type="file" class="form-control" onChange="showPreview(this,'targetLayer0');" />
                                            </div>
                                        </div>


                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="category">Category : </label>
                                        <select name="category" id="category" class="form-control" form="product-form" required>
                                            @foreach($categories as $c)
                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="col-md-6">
                                        <label for="name">Name : </label>
                                        <input class="form-control" id="name" name="name" type="text"  value="{{ old('name') }}" required autofocus>

                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="col-md-6">
                                        <label for="generic-name">Generic name : </label>
                                        <input class="form-control" id="generic-name" name="generic_name" type="text"  value="{{ old('generic_name') }}" >

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="#image-modal" data-toggle="modal" class="btn btn-warning pull-right" onclick="getImage()"><span class="glyphicon glyphicon-picture"> </span> Copy image url in gallery</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="description">Informations : </label>
                                        <textarea id="description" name="description" class="form-control" style="min-height: 100px">
                                            {{ old('description') }}
                                        </textarea>

                                    </div>

                                </div>




                                <button class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add</button>

                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

        <div class="modal fade" id="edit_cat">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" id="edit-product-form" method="POST" action="{{route('update_product')}}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="edit-id" name="id" >

                            <fieldset>

                                <div >


                                    <div class="form-group col-md-6">
                                        <div id="targetOuter">
                                            <div id="targetLayer1">
                                                <img src="{{asset('images/no_image.jpg')}}"  class="icon-choose-image" />
                                            </div>
                                            <div>
                                                <input name="img" id="edit-image"  type="file" class="form-control" onChange="showPreview(this,'targetLayer1');" />
                                            </div>
                                        </div>


                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="edit-category">Category : </label>
                                        <select name="category" id="edit-category" class="form-control" form="edit-product-form" required>
                                            @foreach($categories as $c)
                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="col-md-6">
                                        <label for="edit-name">Name : </label>
                                        <input class="form-control" id="edit-name" name="name" type="text"  required autofocus>

                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="col-md-6">
                                        <label for="edit-generic-name">Generic name : </label>
                                        <input class="form-control" id="edit-generic-name" name="generic_name" type="text"   >

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="#image-modal" data-toggle="modal" class="btn btn-warning pull-right" onclick="getImage()"><span class="glyphicon glyphicon-picture"> </span> Copy image url in gallery</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="edit_content">Informations : </label>
                                        <textarea id="edit_content" name="description" class="form-control" style="min-height: 100px">
                                            {{ old('description') }}
                                        </textarea>

                                    </div>

                                </div>




                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>

                            </fieldset>

                        </form>


                    </div>





                </div>


            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>

    <script>

    function edit_product(id,name,gen_name,imgurl,id_cat) {
        $('#edit-id').val(id);
        $('#edit-name').val(name);
        $('#edit-generic-name').val(gen_name);
        $('#edit-category').val(id_cat);
        if(imgurl.length>0)
            $('#targetLayer1').html('<img src=" '+ imgurl +' "  class="icon-choose-image"/>');
        else
            $('#targetLayer1').html('<img src="{{asset('images/no_image.jpg')}}"  class="icon-choose-image" />');


        CKEDITOR.instances.edit_content.setData('Loading.......');
        getContent('{{route('getcontent_product')}}',id);

    }

    $(function () {

        CKEDITOR.replace( 'description');
        CKEDITOR.replace( 'edit_content');


    });


    function showPreview(objFileInput,elementName) {
        if (objFileInput.files[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $("#"+elementName).html('<img src="'+e.target.result+'" width="150px" height="150px" class="icon-choose-image" />');
                $("#"+elementName).css('opacity','0.8');
            };
            startLoading();
            fileReader.readAsDataURL(objFileInput.files[0]);
            stopLoading();
        }
    }


    function updateFeature(id,option) {
       // startLoading();
        $.ajax({
            url:'{{route('updateFeature_product')}}',
            type:'POST',
            dataType: 'POST',
            data: "_token={{csrf_token()}}&option="+option+"&id="+id,
            cache: false,
            complete: function (res,stat) {
                var spanEl=$('#fc'+id);
                if(res.statusText==='OK')
                {

                    if(option===0){
                        spanEl.html('<a href="#featEl'+ id +'" id="featEl'+ id +'" onclick="updateFeature('+ id +',1)" class="btn btn-success btn-xs"> Set as featured</a>')
                    }else if(option===1) {
                        spanEl.html('<a href="#featEl'+ id +'" id="featEl'+ id +'" onclick="updateFeature('+ id +',0)" class="btn btn-danger btn-xs"> Remove from featured</a>')

                    }
                }else
                {
                    alert(res.statusText);
                }


                //stopLoading();
            }
            
        })

    }

</script>
@endsection

@section('styles')
    <style>
        .icon-choose-image
        {
            width: 150px;
            height: 150px;
            border: solid 1px;
            border-radius: 3px;
        }
        .photoClass
        {
            padding: 5px;
        }
        .nom-for-photo
        {
            font-size: 16px;
        }
    </style>
@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
