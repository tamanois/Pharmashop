@extends('admin-skeleton')
@section('content')
<div class="dashboard-content">
    <div class="container-fluid">
        <div class="cards row">
            <div class="card-item col-md-3 col-sm-6 col-xs-6">

                <div class="square" style="background: #f66b4e">
                    <div class="card-title text-center text-uppercase">
                        <h4 style="font-size: 15px">TOTAL SALES</h4>
                        <hr class="card-hr">

                    </div>
                    <div class="card-amount text-center " style="font-weight: bold">
                        <span>{{$total_sales}}</span> @lang('main.currency')
                    </div>
                </div>
            </div>
            <div class="card-item col-md-3 col-sm-6 col-xs-6">

                <div class="square" style="background: #0da550">
                    <div class="card-title text-center text-uppercase">
                        <h4 style="font-size: 15px">SALES THIS YEAR</h4>
                        <hr class="card-hr">

                    </div>
                    <div class="card-amount text-center " style="font-weight: bold">
                        <span>{{$year_sales}}</span> @lang('main.currency')
                    </div>
                </div>
            </div>
            <div class="card-item col-md-3 col-sm-6 col-xs-6">

                <div class="square" style="background: #49d0f5">
                    <div class="card-title text-center text-uppercase">
                        <h4 style="font-size: 15px">ORDERS</h4>
                        <hr class="card-hr">

                    </div>
                    <div class="card-amount text-center" style="font-weight: bold">
                        <span>{{$nbOrders}}</span>
                    </div>
                </div>
            </div>
            <div class="card-item col-md-3 col-sm-6 col-xs-6">

                <div class="square" style="background: #0071bd">
                    <div class="card-title text-center text-uppercase">
                        <h4 style="font-size: 15px">USERS</h4>
                        <hr class="card-hr">

                    </div>
                    <div class="card-amount text-center " style="font-weight: bold">
                        <span>{{$users->count()}}</span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row stats">
            <div class="col-md-6">
                <h4>Bestsellers</h4>
                <?php $i=1; ?>
                <table class="table table-responsive">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Quantity</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sellers as $s)
                        @foreach($dosages as $d)
                            @if($d->id==$s->id_dosage)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td><strong>{{$d->product->name}} - <span class="text-gray">{{$d->description}}</span></strong></td>
                                    <td><strong>{{$s->quantity}}</strong></td>
                                </tr>
                                <?php break; ?>
                                @endif
                        @endforeach
                        @if($i>=6)
                            <?php break; ?>

                        @endif
                        <?php $i++; ?>

                    @endforeach


                    </tbody>
                </table>
            </div>

        </div>
    </div>


</div>


@endsection

@section('scripts')

@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
