@extends('admin-skeleton')
@section('content')

@if($uOrders>0)
    <div class="container-fluid  alert alert-danger">
        <ul>
            <li class=""><strong class=""> There are {{$uOrders}} paid but undelivered orders</strong></li>

        </ul>

    </div>
@endif
    <div class="notification-container" style="font-family:  Helvetica, Arial">
        @foreach($notifications as $no)
        <div class="notification @if($no->dismissed==0) bg-warning @endif">
            <div class="content">
                <div class="not-image
                @if($no->type==0)
                not-image-order
                    @elseif($no->type==10)
                        not-image-user
                    @endif
                        ">

                </div>
                <div class="not-text" style="margin-top: 15px">
                    <p style="font-size: 15px"><?php echo ($no->message) ?></p>
                    <p class="text-gray" style="font-weight: 600;font-size: 12px"><span class="glyphicon glyphicon-calendar"></span>{{(new DateTime($no->created_at))->format('d/m/Y, H:i')}} </p>
                </div>

            </div>
        </div>

        @endforeach
        @if($notifications->count()<=0)
            <p class="text-gray" style="font-size: 20px"><strong><span class="glyphicon glyphicon-folder-open"></span> Notification folder is empty</strong></p>
            @endif

    </div>
@endsection

@section('scripts')

@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
