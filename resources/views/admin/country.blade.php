@extends('admin-skeleton')
@section('content')
    @include('perso.functions')

    <div class="col-md-6">


        <button class="btn btn-primary" data-toggle="modal" data-backdrop="false" href="#add_cat"
        ><span class="glyphicon glyphicon-plus"></span> New country</button>
        <td><a href="#multi-delete" id="multi-delete" onclick="sendToDelete('{{route('mdelete_country')}}')"  class="btn btn-danger btn-xs pull-right"><span class="glyphicon glyphicon-trash"></span> remove</a></td>

        <table class="table  table-striped table-condensed table-inverse" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Ship</th>
                <th>Price</th>
                <th><input type="checkbox" id="master-check" onchange="master_check_change(this)"> </th>
            </tr>

            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($countries as $c)
                <tr>
                    <td>{{$i++}}</td>
                    <td><a href="#edit_cat" data-toggle="modal" data-backdrop="false" onclick="edit_count({{$c->id}},'{{sanitize($c->name)}}',{{$c->ship}},{{$c->ship_price}})"><strong ><span class="glyphicon glyphicon-edit"></span> {{$c->name}}</strong></a></td>
                    <td>{{($c->ship==1)?'YES':'NO'}}</td>
                    <td><strong>{{($c->ship==1)?$c->ship_price.' '.trans('main.currency'):''}}</strong></td>
                    <td><input type="checkbox" name="delete[]" value="{{$c->id}}" class="check-list" ></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="add_cat">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" id="country-form" method="POST" action="{{route('store_country')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>

                                <div class="form-group">
                                    <label for="name">Name : </label>
                                    <input class="form-control" id="name" name="name" type="text"  value="{{ old('name') }}" required autofocus>

                                </div>

                                <div class="form-group">
                                    <label for="price">Shipping price</label>
                                    <div class="input-group">

                                        <input type="number" id="price" step="0.01" name="price" class="form-control">
                                        <div class="input-group-addon">
                                            @lang('main.currency')
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ship">Ship : </label>
                                    <select name="ship" class="form-control" id="ship" form="country-form">
                                        <option value="1">YES</option>
                                        <option value="0">NO</option>
                                    </select>
                                </div>

                                <button class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add</button>

                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

        <div class="modal fade" id="edit_cat">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" id="edit-country-form" method="POST" action="{{route('update_country')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="edit-id" name="id" >
                            <fieldset>

                                <div class="form-group">
                                    <label for="edit-name">Name : </label>
                                    <input class="form-control" id="edit-name" name="name" type="text"  required >

                                </div>

                                <div class="form-group">
                                    <label for="edit-price">Shipping price</label>
                                    <div class="input-group">

                                        <input type="number" id="edit-price" step="0.01" name="price" class="form-control">
                                        <div class="input-group-addon">
                                            @lang('main.currency')
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-ship">Ship : </label>
                                    <select name="ship" class="form-control" id="edit-ship" form="edit-country-form">
                                        <option value="1">YES</option>
                                        <option value="0">NO</option>
                                    </select>
                                </div>
                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

    </div>

@endsection

@section('scripts')

    <script>

    function edit_count(id,name,ship,price) {
        $('#edit-id').val(id);
        $('#edit-name').val(name);
        $('#edit-ship').val(ship);
        $('#edit-price').val(price);
    }


</script>
@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
