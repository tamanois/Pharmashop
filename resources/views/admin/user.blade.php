@extends('admin-skeleton')
@section('content')
    @include('perso.functions')

    <div class="col-md-12">



        <table class="table  table-striped table-condensed table-inverse" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Type</th>
                <th>Registration</th>
                <th></th>
            </tr>

            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($users as $u)
                <tr>
                    <td >{{$i++}}</td>
                    <td ><a href="#" data-toggle="modal" data-backdrop="false" ><strong > {{$u->forname.' '.$u->name}}</strong></a></td>
                    <td >{{$u->email}}</td>
                    <td >
                        <strong>
                            @if($u->level==1)
                                Administrator
                            @elseif($u->level==2)
                                Customer
                            @endif
                        </strong>

                    </td>
                    <td >
                        {{(new DateTime($u->created_at))->format('d-m-Y')}}
                    </td>
                    <td>
                        @if($u->level==2)
                            <a href="{{route('update_user',$u->id).'?option=1'}}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-lock"> </span> Grant admin rights</a>
                         @elseif($u->level==1)
                        <a href="{{route('update_user',$u->id)}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-lock"> </span> Remove admin rights</a>
                        @endif
                    </td>
                </tr>
                <!--<tr>
                    <td>
                        <a href="" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-envelope"> </span> Send email</a>
                    </td>
                </tr>-->
            @endforeach
            </tbody>
        </table>



        <div class="modal fade" id="edit_user">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" method="POST" action="{{route('update_category')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="edit-id" name="id" >
                            <fieldset>

                                <div class="form-group">
                                    <label for="edit-name">Name : </label>
                                    <input class="form-control" id="edit-name" name="name" type="text"  required >

                                </div>
                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

    </div>

@endsection

@section('scripts')
<script>
    function edit_user(id,name) {
        $('#edit-id').val(id);
        $('#edit-name').val(name);
    }


</script>
@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
