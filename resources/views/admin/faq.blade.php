@extends('admin-skeleton')
@section('content')
    @include('perso.functions')

    <div class="col-md-10">


        <button class="btn btn-primary" data-toggle="modal" data-backdrop="false" href="#add_cat"
        ><span class="glyphicon glyphicon-plus"></span> New FAQ</button>
        <td><a href="#multi-delete" id="multi-delete" onclick="sendToDelete('{{route('mdelete_faq')}}')"  class="btn btn-danger btn-xs pull-right"><span class="glyphicon glyphicon-trash"></span> remove</a></td>

        <table class="table  table-striped table-condensed table-inverse" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>#</th>
                <th style="min-width: 200px">Title</th>
                <th>Content</th>
                <th>Category</th>
                <th><input type="checkbox" id="master-check" onchange="master_check_change(this)"> </th>
            </tr>

            </thead>
            <tbody>

            <?php $i=1; ?>
            @foreach($faqs as $f)
                <tr>
                    <td>{{$i++}}</td>
                    <td><a href="#edit_cat" data-toggle="modal" data-backdrop="false" onclick='edit_faq({{$f->id}},"{{sanitize($f->title)}}","{{$f->id_category}}")'><strong ><span class="glyphicon glyphicon-edit"></span> {{$f->title}}</strong></a></td>
                    <td>
                      Click edit to see
                        </td>
                    <td>{{$f->category?$f->category->name:''}}</td>
                    <td><input type="checkbox" name="delete[]" value="{{$f->id}}" class="check-list" ></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="add_cat">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div  class="modal-body">



                        <form accept-charset="UTF-8" role="form" id="country-form" method="POST" action="{{route('store_faq')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>

                                <div class="form-group col-md-6">
                                    <label for="category">Category: </label>
                                    <select name="category" id="category" class="form-control">
                                        @foreach($categories as $c)
                                            <option value="{{$c->id}}" @if(old('category')==$c->id) selected  @endif>{{$c->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="title">Title : </label>
                                        <input class="form-control" id="title" name="title" type="text"  value="{{ old('title') }}" required autofocus>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="#image-modal" data-toggle="modal" class="btn btn-warning pull-right" onclick="getImage()"><span class="glyphicon glyphicon-picture"> </span> Copy image url in gallery</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea name="content" id="content" >{{old('content')}}</textarea>
                                </div>




                                <button class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add</button>

                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

        <div class="modal fade" id="edit_cat">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" id="edit-country-form" method="POST" action="{{route('update_faq')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="edit-id" name="id" >
                            <fieldset>

                                <div class="form-group col-md-6">
                                    <label for="category">Category: </label>
                                    <select name="category" id="edit-category" class="form-control">
                                        @foreach($categories as $c)
                                            <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="edit-title">Title : </label>
                                        <input class="form-control" id="edit-title" name="title" type="text"  required >

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="#image-modal" data-toggle="modal" class="btn btn-warning pull-right" onclick="getImage()"><span class="glyphicon glyphicon-picture"> </span> Copy image url in gallery</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-content">Content</label>
                                    <textarea name="content" id="edit_content" >

                                    </textarea>
                                </div>


                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>

    <script>

    function edit_faq(id,title,category) {
        $('#edit-id').val(id);
        $('#edit-title').val(title);
        $('#edit-category').val(category);

        CKEDITOR.instances.edit_content.setData('Loading.......');

        getContent('{{route('getcontent_faq')}}',id);


    }


    $(function () {

        CKEDITOR.replace( 'content');
        CKEDITOR.replace( 'edit_content');


    });



    </script>
@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
