@extends('admin-skeleton')
@section('content')

    <div class="col-md-6">
        <h4>Product: {{$product->name}}</h4>

        <button class="btn btn-primary" data-toggle="modal" data-backdrop="false" href="#add_cat"
        ><span class="glyphicon glyphicon-plus"></span> New Strength</button>
        <td><a href="#multi-delete" id="multi-delete" onclick="sendToDelete('{{route('mdelete_dosage')}}')"  class="btn btn-danger btn-xs pull-right"><span class="glyphicon glyphicon-trash"></span> remove</a></td>

        <table class="table  table-striped table-condensed table-inverse" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>#</th>
                <th>Description</th>
                <th>Price</th>

                <th><input type="checkbox" id="master-check" onchange="master_check_change(this)"> </th>
            </tr>

            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($Dosages as $c)
                <tr>
                    <td>{{$i++}}</td>
                    <td><a href="#edit_cat" data-toggle="modal" data-backdrop="false" onclick="edit_dosage({{$c->id}},'{{$c->description}}','{{$c->price}}')"><strong ><span class="glyphicon glyphicon-edit"></span> {{$c->description}}</strong></a></td>
                    <td><strong>{{$c->price.' '.trans('main.currency') }}</strong></td>
                    <td><input type="checkbox" name="delete[]" value="{{$c->id}}" class="check-list" ></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="add_cat">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" method="POST" action="{{route('store_dosage',$product->id)}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>

                                <div class="form-group">
                                    <label for="description">Description : </label>
                                    <input class="form-control" id="description" name="description" type="text"  value="{{ old('description') }}" required AUTOFOCUS>

                                </div>
                                <div class="form-group">
                                    <label for="price">Price : </label>
                                    <div class="input-group">
                                        <input class="form-control" id="price" name="price" type="number" step="0.01"  value="{{ old('price') }}" required>

                                        <div class="input-group-addon">
                                            @lang('main.currency')
                                        </div>
                                    </div>

                                </div>

                                <button class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add</button>

                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

        <div class="modal fade" id="edit_cat">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" method="POST" action="{{route('update_dosage',$product->id)}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="edit-id" name="id" >
                            <fieldset>

                                <div class="form-group">
                                    <label for="description">Description : </label>
                                    <input class="form-control" id="edit-description" name="description" type="text"   required >

                                </div>
                                <div class="form-group">
                                    <label for="price">Price : </label>
                                    <div class="input-group">
                                        <input class="form-control" id="edit-price" name="price" type="number" step="0.01"   required>

                                        <div class="input-group-addon">
                                            @lang('main.currency')
                                        </div>
                                    </div>

                                </div>
                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

    </div>

@endsection

@section('scripts')
<script>
    function edit_dosage(id,description,price) {
        $('#edit-id').val(id);
        $('#edit-description').val(description);
        $('#edit-price').val(price);
    }


</script>
@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
