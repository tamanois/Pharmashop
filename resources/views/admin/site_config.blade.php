@extends('admin-skeleton')
@section('content')
    <h3>Banner</h3>

    <div style="min-height: 300px" class="container-fluid col-md-12">
        <form action="{{route('save_banner')}}" id="banner-form" enctype="multipart/form-data" method="post">
            {{csrf_field()}};
            <div class="row-fluid">
                <div id="img-banner" style="padding: 5px 10px;" class="col-md-6" >
                    <h4>Image</h4>

                    <div >


                        <div class="">
                            <div id="targetOuter">
                                <div id="targetLayer1">
                                    @if(isset($img))
                                        <img style="" src="{{asset($img->value)}}"  class="icon-choose-image" />

                                    @else
                                        <img style="" src="{{asset('images/no_image.jpg')}}"  class="icon-choose-image" />

                                    @endif
                                </div>
                                <div>
                                    <input name="img" id="edit-image"  type="file" class="form-control" onChange="showPreview(this,'targetLayer1');" />
                                </div>
                            </div>


                        </div>

                    </div>
                    <h4>Ads bar</h4>
                    <div class="form-group">
                        <input type="checkbox" name="ads_visible" value="true" @if(isset($ads_visible)) @if($ads_visible->value==1) checked @endif @endif> <label for="">Show ads bar</label>

                    </div>
                    <div class="form-group">
                        <label for="">Ads bar text</label>
                        <textarea name="ads_text" class="form-control" placeholder="@if(isset($ads_text)) {{$ads_text->value}} @endif"></textarea>

                    </div>
                </div>
                <div style="padding: 5px 10px" class="col-md-6">
                    <h4>Banner text</h4>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Title :</label>
                            <input type="text" name="title" class="form-control" placeholder="@if(isset($batitle)) {{$batitle->value}} @endif">
                        </div>
                        <div class="form-group">
                            <label>Text :</label>
                            <textarea name="text" class="form-control" form="banner-form" placeholder="@if(isset($batext)) {{$batext->value}} @endif"></textarea>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label>Button text :</label>
                                <input name="btext" class="form-control"  placeholder="@if(isset($btext)) {{$btext->value}} @endif"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Button url :</label>
                                <textarea name="burl" class="form-control" form="banner-form" placeholder="@if(isset($burl)) {{$burl->value}} @endif"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row-fluid">
                <div class="col-md-12">
                    <button class="btn btn-success pull-right  btn-lg"><span class="glyphicon glyphicon-ok-sign"></span> Save</button>

                </div>
            </div>

        </form>

    </div>

@endsection

@section('styles')
    <style>
        #targetOuter img
        {
            height: 200px;
        }
        #img-banner
        {
            border-right: solid 2px lightgrey ;
        }
        @media (max-width: 767px)
        {
            #img-banner
            {
                border-right: none ;
            }
        }
    </style>
@endsection

@section('scripts')

@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
