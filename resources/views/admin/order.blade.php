@extends('admin-skeleton')
@section('content')
    @include('perso.functions')

    <div class="col-md-12">



        <table class="table  table-striped table-condensed table-inverse" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>#</th>
                <th>Order</th>
                <th>User</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Date</th>
                <th>Shipped ?</th>
                <th></th>
            </tr>

            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($orders as $o)
                <tr>
                    <td >{{$i++}}</td>
                    <td ><a href="{{route('get_order',$o->id)}}" data-toggle="modal" data-backdrop="false" ><strong ><span class="glyphicon glyphicon-search"> </span>  #{{sprintf('%08d',$o->id)}}</strong></a></td>
                    <td >@if($o->id_user!=0)
                                {{$o->user->forname.' '.$o->user->name.' ('.$o->user->email.')'}}
                             @else
                            {{$o->email}}

                        @endif
                    </td>
                    <td >
                        <strong class="text-info">
                           {{$o->amount}} @lang('main.currency')
                        </strong>

                    </td>
                    <td>

                        @if($o->status==5)
                            <strong class="text-success">
                                Paid
                            </strong>
                            @elseif($o->status==4)
                            <strong class="text-info">
                                Confirming
                            </strong>
                        @elseif($o->status==3)
                            <strong class="text-warning">
                                Refunded
                            </strong>
                        @elseif($o->status==2)
                            <strong class="text-danger">
                                Marked Invalid
                            </strong>
                        @elseif($o->status==1)
                            <strong class="text-danger">
                                Canceled/Expired
                            </strong>
                        @elseif($o->status==0)
                            <strong class="text-primary">
                                Pending
                            </strong>
                            @endif
                    </td>
                    <td >
                        {{(new DateTime($o->updated_at))->format('H:i, d-m-Y')}}
                    </td>
                    <td>
                       @if($o->checked==0)
                           <strong>
                               No
                           </strong>

                        @else
                          <strong>
                              Yes
                          </strong>
                        @endif
                    </td>
                    <td>
                        @if($o->status==5)
                            @if($o->checked==0)
                                <a href="{{route('update_order_a',$o->id).'?option=1'}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok-sign"></span> Mark delivered</a>

                            @else
                                <a href="{{route('update_order_a',$o->id).'?option=0'}}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove-circle"></span> Mark undelivered</a>

                            @endif
                            @endif

                    </td>
                </tr>
                <!--<tr>
                    <td>
                        <a href="" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-envelope"> </span> Send email</a>
                    </td>
                </tr>-->
            @endforeach
            </tbody>
        </table>



        <div class="modal fade" id="edit_user">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div
                            class="modal-body">



                        <form accept-charset="UTF-8" role="form" method="POST" action="{{route('update_category')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="edit-id" name="id" >
                            <fieldset>

                                <div class="form-group">
                                    <label for="edit-name">Name : </label>
                                    <input class="form-control" id="edit-name" name="name" type="text"  required >

                                </div>
                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                            </fieldset>
                        </form>


                    </div>





                </div>


            </div>
        </div>

    </div>

@endsection

@section('scripts')
<script>
    function edit_user(id,name) {
        $('#edit-id').val(id);
        $('#edit-name').val(name);
    }


</script>
@endsection
<!--<div class="paginate" style="text-align: center;"> <?php //echo(str_replace('/?', '?', $category->render()) ); ?></div>-->
