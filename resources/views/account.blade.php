﻿@extends('skeleton')


@section('d-content')

	<div class="account-container">
        <div class="title col-md-3 col-sm-5">
            <div class="content">
                <div class="icon">
                    <img src="{{asset('images/user.png')}}" alt="">
                </div>
                <div class="title-list">
                    <div class="text-center">
                        <h3>{{$user->forname.' '.$user->name}}</h3>
                        <small class="text-primary"><strong>{{$user->email}}</strong></small>
                        <hr>


                    </div>
                    <div class="" style="margin-left: -2px ;margin-right: -2px">
                        <h4 class="account-title-item @if(isset($infos)) active @endif"><a href="{{route('account')}}"><span class="glyphicon glyphicon-list-alt"></span> Personnals informations</a></h4>
                        <hr style="margin-top: 0;margin-bottom: 0">
                        <h4 class="account-title-item @if(isset($address)) active @endif"><a href="{{route('account_addresses')}}"><span class="glyphicon glyphicon-phone-alt"></span> Addresses</a></h4>
                        <hr style="margin-top: 0;margin-bottom: 0">
                        <h4 class="account-title-item @if(isset($order)) active @endif"><a href="{{route('account_orders')}}"><span class="glyphicon glyphicon-shopping-cart"></span> Orders</a></h4>
                        <hr style="margin-top: 0;margin-bottom: 0">
                        <div class="text-center" style="margin-top: 15px;margin-bottom: 10px">
                            <a href="{{route('logout')}}" class="btn btn-danger">Log out</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="infos col-md-9 col-sm-7">
            <div class="content">
            <h3><strong>@if(isset($account_title)) {{$account_title}} @endif</strong></h3>
                <hr style="margin-top: 0">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                     <ul>
                         @foreach($errors->all() as $e)
                         <li>{{$e}}</li>
                         @endforeach
                     </ul>
                    </div>
                @endif
                <div class="container-fluid">
                    <div class="row" style="padding-bottom: 50px">

                        @yield('account-section')

                    </div>
                </div>

            </div>
        </div>





	</div>

@endsection

@section('styles')
		<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
            body
            {
                background: #dff0d8;
            }
		</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>



	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection