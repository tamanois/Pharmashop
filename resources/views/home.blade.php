﻿@extends('sidebar-skeleton')
@section('home-caroussel')

@endsection

@section('content')
	@if(isset($ads_visible)) @if($ads_visible->value==1)
		<div class="container-fluid ads-ban text-center hidden-xs" style="margin-bottom: 5px;z-index:1000">

			<span class="glyphicon glyphicon-cog"></span> @if(isset($ads_text)) {{$ads_text->value}} @endif
		</div>
	@endif
	@endif

	<div id="carouselBlk">

		<div id="myCarousel" class="carousel slide">
			<div class="carousel-inner">


				<div class="item active">
					<div class="container-fluid dark-bg">

					</div>
					<div class="container-fluid" style="padding-left: 0;padding-right: 0;">
						@if(isset($img))
							<a href="{{route('register')}}"><img style="width: 100%" src="{{asset($img->value)}}" alt="special offers"/></a>

						@endif
						<div class="carousel-caption" style="">
							<h2 class="text-uppercase"><strong>@if(isset($batitle)) {{$batitle->value}} @endif</strong></h2>
							<p  style="font-size: 16px">@if(isset($batext)) {{$batext->value}} @endif</p>
							<a href="@if(isset($burl)) {{$burl->value}} @endif" class="carousel-btn text-center text-uppercase">
								<span class="border">
									@if(isset($btext)) {{$btext->value}} @endif
								</span>
							</a>
						</div>
					</div>
				</div>



			</div>




		</div>
	</div>

	@if(isset($ads_visible)) @if($ads_visible->value==1)
		<div class="container-fluid ads-ban text-center visible-xs" style="margin-bottom: 5px;z-index:1000">

			<span class="glyphicon glyphicon-cog"></span> @if(isset($ads_text)) {{$ads_text->value}} @endif
		</div>
	@endif
	@endif
	@if($dosages->count()>0)
		<div >
			<div class>
				<div class="well well-small">
					<h3>Featured Products <small class="pull-right"></small></h3>
					<div class="row-fluid">
						<div id="featured" class="carousel slide">
							<div class="carousel-inner" >
                                <?php $j=0 ?>
								@foreach($dosages as $d)
								@if($j%3==0)
								@if($j!=0)
								</ul>
							</div>
							@endif
							<div class="item @if($j==0)active @endif">
								<ul class="thumbnails">
									@endif

									<li class="col-md-4 col-sm-6">
										<div class="thumbnail">
											@if(date_diff((new DateTime()),(DateTime::createFromFormat('Y-m-d H:i:s',$d->created_at)))->days<=15)
												<i class="tag"></i>

											@endif
											<a href="{{route('product_details',$d->product->id)}}">
												<div style="padding: 10px;border-radius:5px;border:#ddd solid 1px;width: 120px;margin-left: auto;margin-right: auto" >
													<img src="{{asset($d->product->imgurl)}}" alt=""/>

												</div>
											<div class="caption">
												<h5 class="text-uppercase" style="font-weight: 600">{{$d->product->name}}</h5>
												<div class="container-fluid">
													<small class=" pull-right text-gray">{{$d->description}}</small>

												</div>
												<div class="container-fluid">
													<h4><a class="btn btn-default" href="{{route('product_details',$d->product->id)}}"><strong>VIEW</strong></a> <span class="pull-right" style="margin-top: 7px"> <strong><span>{{$d->price}}</span> @lang('main.currency')</strong></span></h4>

												</div>
											</div>
										</div>
									</li>
                                    <?php $j++ ?>
									@endforeach
								</ul>
							</div>



						</div>
						<a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
						<a class="right carousel-control" href="#featured" data-slide="next">›</a>
					</div>
				</div>
			</div>
		</div>
	@endif
	<h3 style="margin-bottom: 0; "> @if(isset($page_title)){{$page_title}}@endif</h3>
	<hr class="soft"/>
	<div class="row thumbnails" style="margin-left: -15px">
        <?php $n=0; ?>
		@foreach($products as $p)

			<div class="col-md-4 col-sm-6">
				<div class="thumbnail">
					@if(date_diff((new DateTime()),(DateTime::createFromFormat('Y-m-d H:i:s',$p->created_at)))->days<=15)
						<i class="tag"></i>

					@endif
					<a  href="{{route('product_details',$p->id)}}">
						<div style="padding: 10px;border-radius:5px;border:#ddd solid 1px;width: 120px;margin-left: auto;margin-right: auto" >
													<img src="{{asset($p->imgurl)}}" alt=""/>

						</div>
					<div class="caption">
						<h5 style="margin-bottom: 0;font-weight: 600"><a href="{{route('product_details',$p->id)}}" class="text-uppercase"> {{$p->name}}</a></h5>
						<p class="text-gray" style="font-size: 11px;margin-bottom: 5px">
							@if(isset($p->generic_name))
								{{$p->generic_name}}
							@else
								<span style="color: rgba(255,255,255,0)">rien</span>
							@endif
						</p>
						<div class="center-block">
							<div style="margin-left: auto;margin-right:auto;max-width: 200px">
								<div class="small text-gray">
									Strength
								</div>
							</div>
							<select  id="dosage{{$n}}" class="form-control " style="margin-left: auto;margin-right:auto;height: auto;font-size: 14px;max-width: 200px" onchange="dosageChange(this,'price{{$n}}')">
								@foreach($p->dosages as $d)

									<option id="{{$d->id}}" value="{{$d->price}}">{{$d->description}}</option>

								@endforeach
							</select>

						</div>





						<h4 style="text-align:center">
							<a class="btn btn-success" onclick="showCcartModal('dosage{{$n}}')" style="margin-bottom: 2px">Add to <i class="glyphicon glyphicon-shopping-cart"></i></a>
							<a class="btn disabled btn-default" href="#" id="price{{$n}}" style="font-weight: 600">{{($price=$p->dosages->first())?$price->price:0}} @lang('main.currency')</a></h4>
					</div>
				</div>
			</div>
            <?php $n++; ?>
		@endforeach


	</div>
	<div class="row text-center" style="margin-top: 20px;margin-bottom: 20px">
		<!--<strong class="alert alert-info"><a href="{{route('search_product')}}">Show all -></a></strong>-->
	</div>

	</div>
@endsection
@section('d2-content')
	<div class="container-fluid" id="about-c" style="margin-top: 100px;font-size: 2rem">
		<section class="bg-primary" style="background: #8bc443" id="about">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-2 col-md-8 mx-auto text-center">
						<h2 class="section-heading text-white">We've got what you need!</h2>
						<hr class="light my-4 special">
						<p class="text-faded mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor dolores ducimus eveniet, excepturi exercitationem explicabo libero maxime minima mollitia officiis porro quae quasi recusandae reiciendis!</p>
						<a class="btn btn-light btn-xl js-scroll-trigger special" href="{{route('login')}}">Get Started!</a>
					</div>
				</div>
			</div>
		</section>

		<section id="services">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h2 class="section-heading">At Your Service</h2>
						<hr class="my-4 special">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6 text-center">
						<div class="service-box mt-5 mx-auto">
							<i class="fa fa-4x fa-credit-card text-primary mb-3 sr-icons"></i>
							<h3 class="mb-3">Various payment methods</h3>
							<p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur.</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 text-center">
						<div class="service-box mt-5 mx-auto">
							<i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
							<h3 class="mb-3">Many Ship countries</h3>
							<p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit!</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6  col-sm-6 text-center">
						<div class="service-box mt-5 mx-auto">
							<i class="fa fa-4x fa-check-circle text-primary mb-3 sr-icons"></i>
							<h3 class="mb-3">Safe drugs</h3>
							<p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing!</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 text-center">
						<div class="service-box mt-5 mx-auto">
							<i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
							<h3 class="mb-3">Made with Love</h3>
							<p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing.!</p>
						</div>
					</div>
				</div>
			</div>
		</section>



		<section class=" text-white" style="background: #202020">
			<div class="container text-center">
				<h2 class="mb-4" style="color: white">Safe and fast deliveries!</h2>
				<a class="btn btn-light btn-xl sr-button special" href="#">Order now!</a>
			</div>
		</section>

		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-2 col-md-8 mx-auto text-center">
						<h2 class="section-heading">Let's Get In Touch!</h2>
						<hr class="my-4 special">
						<p class="mb-5">You need futher informations? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
					</div>
				</div>
				<div class="row">

					<div class="col-md-offset-2 col-md-4 ml-auto text-center">
						<i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
						<p>123-456-6789</p>
					</div>
					<div class="col-md-4 mr-auto text-center">
						<i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
						<p>
							<a href="mailto:your-email@your-domain.com">feedback@startbootstrap.com</a>
						</p>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('styles')
	<link rel="stylesheet" href="{{asset('for_home/creative.min.css')}}">
	<link rel="stylesheet" href="{{asset('for_home/font1.css')}}">
	<link rel="stylesheet" href="{{asset('for_home/font2.css')}}">

	<style>

		#header
		{
			margin-top: -40px !important;
		}

		#myCarousel .carousel-caption .carousel-btn{
			color: white;
		}
	</style>
	@yield('styles')
@endsection


@section('scripts')
	<script src="{{asset('for_home/creative.min.js')}}"></script>
	<script src="{{asset('for_home/scrollreveal.min.js')}}"></script>

	<script>
        function dosageChange(el,priceEl) {
            var v=el.value;
            $('#'+priceEl).html(v+' €');
        }


	</script>
@endsection

@section('title')
	Home - @lang('main.site_name')
@endsection