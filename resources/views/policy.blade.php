﻿@extends('skeleton')


@section('d-content')
<div class="text-center">
	<h2 class=" text-uppercase"><strong>{{$policy->title}}</strong></h2>
</div>
	<div class="article-container" style="font-weight: 500;margin-top: 75px">
            <?php echo ($policy->content); ?>

	</div>

@endsection

@section('styles')
		<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
		</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>



	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection