@extends('account')
@section('account-section')

    <div class="container-fluid">
        <div class="orders-container" style="padding-left: 10px">
            <?php $pendingTransaction=false ?>
            @foreach($orders as $o)
                <div class="order">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="header" style="display: flex;position: relative">
                                <a href="{{route('get_order',$o->id)}}" ><h3 style="font-weight: 600">N° {{sprintf('%08d',$o->id)}}</h3></a>
                                <span class="" style="font-size: 20px;padding:17px 10px;position: absolute;right: 0">
                        <span class="@if($o->status==0) text-warning @elseif($o->status==5) text-success @else text-danger @endif" style="">
                            <strong id="order{{$o->id}}">
                            @if($o->status==0)
                                <?php $pendingTransaction=true; ?>
                                    <span class="glyphicon glyphicon-cog"> </span> Pending
                                @elseif($o->status==5)
                                    <span class="glyphicon glyphicon-ok-sign"> </span>  Paid
                                @else
                                    <span class="glyphicon glyphicon-remove-circle"> </span> Failed
                                @endif
                            </strong>
                        </span>
                    </span>
                            </div>

                        </div>

                        <div class="panel-body">
                            <div class="body" style="padding-left: 20px">

                                <h4>Date: <span class="text-gray"> {{(new DateTime($o->created_at))->format('H:i, d-m-Y')}}</span> </h4>
                                <h4>Method: <span class="text-gray">@if($o->payment_url=='card_payment') Credit card @else CryptoCurrency @endif</span></h4>
                                <h4>Amount: <span class="text-primary" style="font-size: 24px">{{$o->amount}} @lang('main.currency')</span></h4>
                            </div>
                        </div>
                        @if($o->status==5)
                            <div class="panel-footer">
                                @if($o->checked==1) <h4 class="text-primary">Order Delivered</h4> @else <h4 class="text-danger">Not yet delivered</h4> @endif
                            </div>
                        @endif

                    </div>


                </div>
            @endforeach
            @if($orders->count()<=0)
                <h3 class="text-gray">
                    No orders yet
                </h3>
                @endif



        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function updateOrderState() {
            startLoading();
            $.ajax({
               url:'{{route('update_order_state')}}',
               type:'GET',
                data:'id={{$user->id}}',
                success:function (data) {
                    stopLoading();

                    for(i=0;i<data.length;i++)
                    {
                        if(data[i].state=='paid')
                        {
                            $('#order'+data[i].id).html(' <span class="glyphicon glyphicon-ok-sign"> </span>  Paid');

                        }else
                        {
                            $('#order'+data[i].id).html(' <span class="glyphicon glyphicon-remove-circle"> </span> Failed');
                        }
                    }

                },
                error:function (data) {
                    stopLoading();
                    showAlert('Error when contacting the gateway',1);
                }
            });
        }

            @if($pendingTransaction)
                        updateOrderState();
        @endif

    </script>
@endsection