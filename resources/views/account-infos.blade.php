@extends('account')
@section('account-section')
        <div class="col-md-6">
            <p><strong>First name</strong></p>
            <h4 style="color: grey;padding-left: 10px">{{$user->name}}</h4>
        </div>
        <div class="col-md-6">
            <p><strong>Last name</strong></p>
            <h4 style="color: grey;padding-left: 10px">{{$user->forname}}</h4>
        </div>

        <div class="col-md-12">
            <p><strong>Email</strong></p>
            <h4 style="color: grey;padding-left: 10px">{{$user->email}}</h4>
        </div>

        <div class="col-md-12">
            <a href="#edit_pass" data-toggle="modal" data-backdrop="false" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-lock"></span> Change password</a>
        </div>

        <div class="col-md-12">
            <a href="#edit_infos" data-toggle="modal" data-backdrop="false" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-edit"></span> Edit</a>
        </div>

    <div class="modal fade" id="edit_infos">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Please fill the fields</h4>
                </div>
                <div
                        class="modal-body">



                    <form accept-charset="UTF-8" role="form" id="edit-infos-form" method="POST" action="{{route('update_infos')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <fieldset>
                            <div class="row-fluid">
                                <div class="form-group col-md-6">
                                    <label for="name">First name : </label>
                                    <input class="form-control" id="name" placeholder="{{$user->name}}" name="name" type="text"   >

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="lastname">Last name : </label>
                                    <input class="form-control" id="lastname" placeholder="{{$user->forname}}" name="forname" type="text"   >

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email : </label>
                                    <input class="form-control" id="email" placeholder="{{$user->email}}" value="" name="email" type="text" >

                                </div>
                            </div>

                            <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>


                        </fieldset>
                    </form>


                </div>





            </div>


        </div>
    </div>

        <div class="modal fade" id="edit_pass">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Please fill the fields</h4>
                    </div>
                    <div class="modal-body">

                        <form accept-charset="UTF-8" role="form" id="edit-pass-form" method="POST" action="{{route('update_password')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>
                                <div class="row-fluid">
                                    <div class="form-group col-md-12">
                                        <label for="name">Old password : </label>
                                        <input class="form-control"   name="old_password" type="password"   >

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>New password : </label>
                                        <input class="form-control"  name="new_password" type="password" >

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Re-enter new password : </label>
                                        <input class="form-control"  name="new_password_confirmation" type="password" >


                                    </div>
                                </div>

                                <button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

@endsection