﻿@extends('skeleton')


@section('d-content')
<div class="text-center">
	<h2 class="alert alert-info">{{$article->title}}</h2>
</div>
	<div class="article-container" style="font-weight: 500">
            <?php echo ($article->content); ?>

	</div>

@endsection

@section('styles')
		<style>
			.container-row.row
			{
				margin-left: -15px;
			}
			#header
		{
			margin-top: -20px !important;
		}
		</style>
	@yield('styles')
@endsection

@section('scripts')
	<script>



	</script>
@endsection

@section('title')
	{{$title}} - @lang('main.site_name')
@endsection