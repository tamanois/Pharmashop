<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->default(0);
            $table->string('server_order_id')->nullable();
            $table->float('discount')->default(0);
            $table->integer('id_shipping_address')->default(0);
            $table->integer('id_billing_address')->default(0);
            $table->string('email')->nullable();
            $table->string('currency')->nullable();
            $table->string('pay_amount')->default(0);
            $table->integer('id_cart')->default(0);
            $table->double('amount',20,10);
            $table->mediumText('payment_url')->nullable();
            $table->integer('status')->default(0);
            $table->integer('checked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
