<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('/denied',function (){
    return view('not_auth',['titre'=>'']);
})->name('not_auth');


Route::get('/get/token','HomeController@getToken')->name('get_token');
///////////////Le cart
///
Route::any('/dosage/get','CartController@getDosage')->name('get_dosage');
Route::any('/cart/update','CartController@update')->name('update_cart');
Route::any('/cart/refresh','CartController@refresh')->name('refresh_cart');
Route::any('/cart/remove-product','CartController@remove_product')->name('remove_cart_product');
Route::any('/cart/show','CartController@show')->name('show_cart');
Route::post('/cart/confirm','CartController@confirm')->name('confirm_cart');
Route::get('/cart/adresses','CartController@setPaymentAddresses')->name('cart_address');
Route::post('/cart/payment/proceed/','CartController@proceedPayment')->name('proceed_payment');
Route::get('/order/payment/','CartController@payOptions')->name('choose_pay_option');
Route::get('/order/email-notification/','CartController@orderEmailNotification')->name('notify_order');


//crypto payment
Route::get('/order/payment/proceed/{currency}','CartController@proceedCrypto')->name('proceed_crypto');
Route::get('/order/payment/check/{currency}','CartController@checkOrder')->name('check_order');
Route::get('/order/payment/update-order','CartController@updateOrderState')->name('update_order');
//card payment
Route::get('/order/init-card-payment','CartController@initializeCardPayment')->name('init_card');
Route::post('/order/card-payment/proceed/','CartController@proceedCardPayment')->name('proceed_card');








//Le blog
Route::any('/blog','BlogController@index')->name('blog_home');
Route::any('/blog/article/{id}','BlogController@show')->name('show_article');

//Le faq
Route::any('/faq','FaqController@index')->name('faq_home');
Route::get('/faq/policy/{id}','FaqController@show_policy')->name('policy');


//Les produits
Route::any('/products/details/{id}','ProductController@show')->name('product_details');

//Les contact
Route::get('/contact','HomeController@contact')->name('contact');
Route::post('/contact/send','HomeController@send_mail')->name('send_contact_mail');

//La facture
Route::get('/account/order/bill','AccountController@show_bill')->name('show_bill');



////La recherche de produit
Route::get('/products/search','HomeController@search')->name('search_product');


Route::group(['middleware'=>'guest'],function (){
    Route::get('/auth/login',function (){
        return view('auth.login')->with(['page_title'=>'Login']);
    })->name('login');

    Route::post('/auth/register/check','Auth\RegisterController@check_mail')->name('register_check');

    Route::get('/auth/register',function (){
        return view('auth.register')->with(['page_title'=>'Register']);
    })->name('register');

    Route::post('/auth/login/try','Auth\LoginController@login')->name('login_try');

    Route::post('/auth/register/try','Auth\RegisterController@register')->name('register_try');


    Route::get('/password/email','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
    Route::post('/password/email/send-link','Auth\ForgotPasswordController@sendResetLinkEmail')->name('send_reset_link');
    Route::get('/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset/apply','Auth\ResetPasswordController@reset')->name('reset_password');




});




Route::group(['middleware'=>'auth'],function (){

    ///////////////////////////////////// ADMIN
    Route::group(['middleware'=>'admin'],function (){
        Route::get('admin/dashboard','admin\DashboardController@index')->name('dashboard');


        //--------------Site configuration
        Route::get('admin/site-configuration','admin\parametresController@index')->name('site_config');
        Route::post('admin/site-configuration/save','admin\parametresController@save_banner')->name('save_banner');


        //---------------les categories
        Route::get('admin/category','admin\CategoryController@index')->name('category_management');
        Route::post('admin/category/store','admin\CategoryController@store')->name('store_category');
        Route::post('admin/category/update','admin\CategoryController@update')->name('update_category');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_category');
        Route::post('admin/category/delete/','admin\CategoryController@destroys')->name('mdelete_category');

        //---------------les categories de faq
        Route::get('admin/faq/category','admin\FaqCategoryController@index')->name('faq_category_management');
        Route::post('admin/faq/category/store','admin\FaqCategoryController@store')->name('store_faq_category');
        Route::post('admin/faq/category/update','admin\FaqCategoryController@update')->name('update_faq_category');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_category');
        Route::post('admin/faq/category/delete/','admin\FaqCategoryController@destroys')->name('mdelete_faq_category');


        ////////////////////////////////////
        //---------------les pays
        Route::get('admin/country','admin\CountryController@index')->name('country_management');
        Route::post('admin/country/store','admin\CountryController@store')->name('store_country');
        Route::post('admin/country/update','admin\CountryController@update')->name('update_country');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_country');
        Route::post('admin/country/delete/','admin\CountryController@destroys')->name('mdelete_country');


        ////////////////////////////////////
        ///  ////////////////////////////////////
        //---------------les  Faq
        Route::get('admin/faq','admin\FaqController@index')->name('faq_management');
        Route::post('admin/faq/store','admin\FaqController@store')->name('store_faq');
        Route::post('admin/faq/update','admin\FaqController@update')->name('update_faq');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_country');
        Route::post('admin/faq/delete/','admin\FaqController@destroys')->name('mdelete_faq');
        Route::post('admin/faq/content/','admin\FaqController@getContent')->name('getcontent_faq');



        ////////////////////////////////////
        //---------------les  Policy
        Route::get('admin/policy','admin\PolicyController@index')->name('policy_management');
        Route::post('admin/policy/store','admin\PolicyController@store')->name('store_policy');
        Route::post('admin/policy/update','admin\PolicyController@update')->name('update_policy');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_country');
        Route::post('admin/policy/delete/','admin\PolicyController@destroys')->name('mdelete_policy');
        Route::post('admin/policy/content/','admin\PolicyController@getContent')->name('getcontent_policy');


        ////////////////////////////////////
        //---------------les  Articles
        Route::get('admin/article','admin\ArticleController@index')->name('article_management');
        Route::post('admin/article/store','admin\ArticleController@store')->name('store_article');
        Route::post('admin/article/update','admin\ArticleController@update')->name('update_article');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_country');
        Route::post('admin/article/delete/','admin\ArticleController@destroys')->name('mdelete_article');
        Route::post('admin/article/content/','admin\ArticleController@getContent')->name('getcontent_article');

        ////////////////////////////////////
        //---------------les  produits
        Route::get('admin/product','admin\ProductController@index')->name('product_management');
        Route::post('admin/product/store','admin\ProductController@store')->name('store_product');
        Route::post('admin/product/update','admin\ProductController@update')->name('update_product');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_country');
        Route::post('admin/product/delete/','admin\ProductController@destroys')->name('mdelete_product');
        Route::post('admin/product/description/','admin\ProductController@getContent')->name('getcontent_product');
        Route::post('admin/product/feature/update/','admin\ProductController@updateFeature')->name('updateFeature_product');

        // Les images
        Route::get('admin/image','admin\ImageController@index')->name('image_management');
        Route::post('admin/image/store','admin\ImageController@store')->name('store_image');
        Route::get('admin/image/delete/','admin\ImageController@destroy')->name('delete_image');


        //---------------les  dosages
        Route::get('admin/dosage/{id}','admin\DosageController@index')->name('dosage_management');
        Route::post('admin/dosage/store/{id}','admin\DosageController@store')->name('store_dosage');
        Route::post('admin/dosage/update/{id}','admin\DosageController@update')->name('update_dosage');
        //Route::get('admin/category/delete/{id}','admin\CategoryController@destroy')->name('delete_country');
        Route::post('admin/dosage/delete/','admin\DosageController@destroys')->name('mdelete_dosage');

        // Les Notifications
        Route::get('admin/notifications/','admin\NotificationController@index')->name('notification_management');

        //les utilisateurs
        Route::get('admin/users/','admin\UserController@index')->name('user_management');
        Route::get('admin/user/update-level/{id}','admin\UserController@update')->name('update_user');
        Route::post('admin/user/send-email/','admin\UserController@send_mail')->name('mail_to_user');


        //Les commaandes
        Route::get('admin/orders/','admin\OrderController@index')->name('order_management');
        Route::get('admin/order/update/{id}','admin\OrderController@update')->name('update_order_a');

    });




    ////////////////////////////////////

    ///Le compte
    Route::get('/account','AccountController@index')->name('account');
    Route::post('/account/update/infos','AccountController@update_infos')->name('update_infos');
    Route::post('/account/update/password','AccountController@update_password')->name('update_password');
    Route::get('/account/addresses','AccountController@index_addresses')->name('account_addresses');
    Route::get('/account/orders','AccountController@index_orders')->name('account_orders');
    Route::get('/account/order/{id}','AccountController@get_order')->name('get_order');
    Route::get('account/orders/update-state','AccountController@update_state')->name('update_order_state');


    Route::post('/account/update/billing','AccountController@update_billing')->name('update_billing');
    Route::post('/account/update/shipping','AccountController@update_shipping')->name('update_shipping');








    Route::get('/auth/logout',function (){
        Auth::logout();

        return redirect()->to(URL::previous());
    })->name('logout');
});
