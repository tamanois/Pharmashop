<?php

namespace pharmashop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class orderConfirmMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $pdfPath;
    public function __construct($pdfPath)
    {

        $this->pdfPath=$pdfPath;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $myPdfPath=$this->pdfPath;
        return $this->markdown('mail.notifications.orderMail')->subject('Order confirmation notification')->attach($myPdfPath,[
            'as' => 'Bill.pdf',
            'mime' => 'application/pdf']);
    }
}
