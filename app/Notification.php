<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Notification
 *
 * @property int $id
 * @property string|null $message
 * @property int $type
 * @property int $dismissed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereDismissed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Notification extends Model
{
    //
    protected  $guarded=[];
}
