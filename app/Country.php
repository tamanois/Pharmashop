<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Country
 *
 * @property int $id
 * @property string $name
 * @property int $ship
 * @property float $ship_price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Billing[] $billings
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Shipping[] $shippings
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereShip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereShipPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Country extends Model
{
    public function billings()
    {
        return $this->hasMany(Billing::class,'id_country');
    }

    public function shippings()
    {
        return $this->hasMany(Shipping::class,'id_country');
    }
}
