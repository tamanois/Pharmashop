<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Order
 *
 * @property-read \pharmashop\Billing $billing
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\OrderItem[] $order_items
 * @property-read \pharmashop\Shipping $shipping
 * @property-read \pharmashop\User $user
 * @mixin \Eloquent
 */
class Order extends Model
{
    public function user()
    {

        return $this->belongsTo(User::class,'id_user');
    }

    public function order_items()
    {
        return $this->hasMany(OrderItem::class,'id_order');
    }

    public function billing()
    {

        return $this->belongsTo(Billing::class,'id_billing_address');
    }

    public function shipping()
    {

        return $this->belongsTo(Shipping::class,'id_shipping_address');
    }
}
