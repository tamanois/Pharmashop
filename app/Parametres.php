<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Parametres
 *
 * @property int $id
 * @property string $name
 * @property string|null $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereValue($value)
 * @mixin \Eloquent
 */
class Parametres extends Model
{

    protected $guarded=[];
}
