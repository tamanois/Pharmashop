<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Cart_product
 *
 * @property int $id
 * @property int $id_cart
 * @property int $id_dosage
 * @property int $quantity
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Cart $cart
 * @property-read \pharmashop\Dosage $dosage
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereIdCart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereIdDosage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Cart_product extends Model
{

    protected $guarded = [];


    public function cart()
    {
        return $this->belongsTo(Cart::class,'id_cart');
    }
    public function dosage()
    {
        return $this->belongsTo(Dosage::class,'id_dosage');
    }
}
