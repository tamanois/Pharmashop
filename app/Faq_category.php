<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Faq_category
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Faq[] $faqs
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Faq_category extends Model
{
    public function faqs()
    {
        return $this->hasMany(Faq::class,'id_faq_category');
    }
}
