<?php
/**
 * Created by PhpStorm.
 * User: Joel
 * Date: 27/07/2018
 * Time: 23:31
 */

namespace pharmashop;


use Illuminate\Http\Request;

class DataCheck
{


    // Products/////////////////////////////////////
    public static function  searchProductByName($name){
        $p=Product::with('dosages')->whereRaw('lower(name) LIKE ?',[strtolower('%'.$name.'%')])
            ->orWhereRaw('lower(generic_name) LIKE ?',[strtolower('%'.$name.'%')])
            ->orderBy('name')->orderBy('generic_name')
            ->get();
        return $p;

    }

    public static function  searchProductByLetter($letter){
        $p=Product::with('dosages')
            ->whereRaw('lower(name) LIKE ?',[strtolower($letter.'%')])
            ->orWhereRaw('lower(generic_name) LIKE ?',[strtolower($letter.'%')])
            ->orderBy('name')->orderBy('generic_name')
            ->get();
        return $p;

    }
    public static function  searchProductByCategory($id){
        $p=Product::with('dosages')->where('id_category','=',$id)
            ->orderBy('name')->orderBy('generic_name')
            ->get();
        return $p;

    }


    public static function  getLastProducts(){
        $p=Product::with('dosages')->limit(20)->orderBy('created_at','desc')->orderBy('name')->get();
        return $p;

    }
    public static function  getAllProducts(){
        $p=Product::with('dosages')->orderBy('name')->get();
        return $p;

    }
    /////////////////////////////////
    /// Dosages
    public static  function  getDosage($id){
       $d=Dosage::with(['product'])->find($id);
       return $d;
    }

    public static  function  getFeaturedProducts(){
        $d=Dosage::with(['product'])->whereHas('product',function ($query){
            $query->where('feature','=',1);
        })->get();

        $d=$d->shuffle();
        return $d;
    }

    ////////////
    /// cart
    public static  function  getCart($id){
       $c=Cart::with('cart_products.dosage')->find($id);

        return $c;
    }
}