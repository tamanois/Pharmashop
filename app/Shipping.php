<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Shipping
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_country
 * @property string $address
 * @property string $city
 * @property string $postal_code
 * @property string $phone
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Country $country
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Shipping extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class,'id_user');
    }

    public function country()
    {
        return $this->belongsTo(Country::class,'id_country');
    }
}
