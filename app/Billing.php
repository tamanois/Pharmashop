<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Billing
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_country
 * @property string $address
 * @property string $city
 * @property string $postal_code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Country $country
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Billing extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class,'id_user');
    }

    public function country()
    {
        return $this->belongsTo(Country::class,'id_country');
    }
}
