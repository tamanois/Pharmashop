<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Dosage
 *
 * @property int $id
 * @property int $id_product
 * @property string|null $description
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Dosage extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class,'id_product');
    }
}
