<?php namespace pharmashop\Http\Middleware;

use Closure;

class g_admin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if($request->user()->level>1){

            return redirect()->route('not_auth');
        }
		return $next($request);
	}

}
