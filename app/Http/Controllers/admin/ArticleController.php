<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Article;
use pharmashop\Http\Controllers\Controller;
use pharmashop\User;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage Articles';
    }

    public function index()
    {

        $c=Article::with('author')->orderBy('title')->get();
        $this->values['articles']=$c;
        return view('admin.article',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title'=>'required',
                'summary'=>'required',
                'content'=>'required',


            ]);


        $title=$request->input('title');
        $summary=$request->input('summary');
        $content=$request->input('content');


        $c=new Article();

        $c->title=$title;
        $c->summary=$summary;
        $c->content=$content;
        $c->id_user=\Auth::user()->id;

        $c->save();
        return redirect()->route('article_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'title'=>'required',
                'summary'=>'required',
                'content'=>'required',
            ]);

        $id=$request->input('id');
        $title=$request->input('title');
        $summary=$request->input('summary');
        $content=$request->input('content');


        $c=Article::find($id);

        $c->title=$title;
        $c->summary=$summary;
        $c->content=$content;
        $c->save();
        return redirect()->route('article_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return redirect()->route('article_management')->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');

        Article::destroy($idList);
        return response()->json('OK');



    }
    public function getContent(Request $request)
    {
        $id=$request->input('id');
        $c=Article::find($id);

        return response($c->content);



    }
}
