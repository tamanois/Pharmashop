<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Faq_category;
use pharmashop\Http\Controllers\Controller;

class FaqCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage categories of Faqs';
    }

    public function index()
    {
        $c=Faq_category::orderBy('name')->get();
        $this->values['categories']=$c;
        return view('admin.faq_category',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            ['name'=>'required|unique:faq_categories']);
        $name=$request->input('name');

        $c=new Faq_category();
        $c->name=$name;
        $c->save();
        return redirect()->route('faq_category_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'name'=>'required|unique:faq_categories'
            ]);
        $id=$request->input('id');
        $name=$request->input('name');

        $c=Faq_category::find($id);
        $c->name=$name;
        $c->save();
        return redirect()->route('faq_category_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq_category::destroy($id);
        return redirect()->route('faq_category_management')->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');

        Faq_category::destroy($idList);
        return response()->json('OK');



    }
}
