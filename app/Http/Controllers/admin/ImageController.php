<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Image;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i= Image::orderBy('created_at','desc')->get();
        return response()->json($i);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=\Validator::make($request->all(),
            [
                'img'=>'Image|max:2048|required'
            ]);
        if($validator->fails()){
            $errors=$validator->errors();
            $errors=$errors->all();

            return response()->json($errors,403);
        }
        $img=$request->file('img');
        if($img)
        {
            $i= new Image();

            $name=$img->getClientOriginalName();
            $i->url=asset('images/upload/'.$name);
            $i->thumbUrl=asset('images/upload/thumbnails/'.$name);
            $i->name=$name;
            $i->save();


            \Intervention\Image\Facades\Image::make($img)->save('images/upload/'.$name);
            \Intervention\Image\Facades\Image::make($img)->resize(200,200,function($constraint)
            {
                $constraint->aspectRatio();
            })->resizeCanvas(200,200)->save('images/upload/thumbnails/'.$name);

            return redirect()->route('image_management');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id=$request->input('id');
        $i=Image::find($id);
        @unlink('images/upload/'.$i->name);
        @unlink('images/upload/thumbnails/'.$i->name);
        $i->delete();
        return redirect()->route('image_management');

    }
}
