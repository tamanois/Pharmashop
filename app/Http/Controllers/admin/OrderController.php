<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Order;

class OrderController extends Controller
{
    protected $values=[];

    public function __construct()
    {
        $this->values['big_title']='Management';

        $this->values['title']='Manage orders';
    }

    public function index()
    {

        $o=Order::with('user')->orderBy('created_at','desc')->get();
        $this->values['orders']=$o;

        return view('admin.order',$this->values);
    }

    public function update(Request $request,$id){
        $deliv=$request->input('option');
        $o=Order::find($id);
        $o->checked=$deliv;
        $o->save();
        return redirect()->route('order_management')->with(['success'=>'ok']);

    }
}
