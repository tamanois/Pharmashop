<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Policy;

class PolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage policies';
    }

    public function index()
    {

        $c=Policy::orderBy('title')->get();
        $this->values['policies']=$c;
        return view('admin.policy',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title'=>'required',
                'content'=>'required',

            ]);
        $title=$request->input('title');
        $content=$request->input('content');

        $c=new Policy();

        $c->title=$title;
        $c->content=$content;
        $c->save();
        return redirect()->route('policy_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'title'=>'required',
                'content'=>'required',

            ]);
        $id=$request->input('id');
        $title=$request->input('title');
        $content=$request->input('content');



        $c=Policy::find($id);
        $c->title=$title;
        $c->content=$content;

        $c->save();
        return redirect()->route('policy_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Policy::destroy($id);
        return redirect()->route('policy_management')->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');

        Policy::destroy($idList);
        return response()->json('OK');



    }

    public function getContent(Request $request)
    {
        $id=$request->input('id');

        $c=Policy::find($id);

        return response($c->content);



    }
}
