<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Http\Controllers\Controller;
use pharmashop\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];

    public function __construct()
    {
        $this->values['big_title']='Management';

        $this->values['title']='Manage users';
    }

    public function index()
    {

        $currentU=\Auth::user();
        $u=User::where('id','<>',$currentU->id)->where('level','>',0)->orderBy('level')->orderBy('created_at','desc')->get();
        $this->values['users']=$u;

        return view('admin.user',$this->values);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send_mail()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u=User::find($id);
        $option=$request->input('option');
        if($option==1)
        {
            $u->level=1;
        }
        else
        {
            $u->level=2;
        }
        $u->save();

        return redirect()->route('user_management')->withSuccess(['ok'=>'']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
