<?php

namespace pharmashop\Http\Controllers\admin;

use DateTime;
use Illuminate\Http\Request;
use pharmashop\Dosage;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Order;
use pharmashop\OrderItem;
use pharmashop\User;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    protected $ySales=0;
    public function __construct()
    {
        $this->values['big_title']='Dashboard';

        $this->values['title']='Dashboard';
        $this->values['isDashboard']=true;

    }
    public function index()
    {
        $u=User::all();
        $oc=Order::count();
        $sales=Order::where('status','=',5)->get();
        $aSales=$sales->sum('amount');

            $this->ySales=0;
            $sales->each(function($s){
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $s->updated_at);
            if($date->format('Y')===date('Y'))
                $this->ySales+=$s->amount;

        });

            $pi=OrderItem::all();
            $pi2=OrderItem::select(['id_dosage'])->distinct()->get();

            $sellers=[];
            $i=0;
            foreach ($pi2 as $oi){
                $sum=$pi->where('id_dosage','=',$oi->id_dosage)->sum('quantity');
                $sellers[$i]=new Seller($oi->id_dosage,$sum);
                $i++;
            }

        $sellers=Seller::sort($sellers);




        $d=Dosage::with(['product'])->get();
            $this->values['dosages']=$d;
            $this->values['sellers']=$sellers;
            $this->values['dosages']=$d;
            $this->values['year_sales']=$this->ySales;
        $this->values['total_sales']=$aSales;
        $this->values['users']=$u;
        $this->values['nbOrders']=$oc;

        return view('admin.dashboard',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
class Seller{
    public $id_dosage;
    public $quantity;

    /**
     * Seller constructor.
     * @param $id_dosage
     * @param $quantity
     */
    public function __construct($id_dosage, $quantity)
    {
        $this->id_dosage = $id_dosage;
        $this->quantity = $quantity;
    }

    public static function sort($sellers){
        $n=sizeof($sellers);

        for($i=0;$i<$n-1;$i++){
            for ($j=$i+1;$j<$n;$j++){
                if ($sellers[$i]->quantity<$sellers[$j]->quantity){
                    $c=$sellers[$i];
                    $sellers[$i]=$sellers[$j];
                    $sellers[$j]=$c;
                }
            }
        }

        return $sellers;

    }
}

