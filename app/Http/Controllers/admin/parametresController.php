<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Parametres;

class parametresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Site configuration';

        $this->values['title']='Settings';
        $this->values['isDashboard']=true;

    }
    public function index()
    {
        $p=Parametres::all();
        $imgurl=$p->where('name','=','banner_image')->first();
        if($imgurl)
            $this->values['img']=$imgurl;

        $ads_visible=$p->where('name','=','show_ads')->first();
        if($ads_visible)
            $this->values['ads_visible']=$ads_visible;

        $ads_text=$p->where('name','=','ads_text')->first();
        if($ads_text)
            $this->values['ads_text']=$ads_text;

        $title=$p->where('name','=','banner_title')->first();
        if($title)
            $this->values['batitle']=$title;

        $text=$p->where('name','=','banner_text')->first();
        if($text)
            $this->values['batext']=$text;

        $btext=$p->where('name','=','button_text')->first();
        if($btext)
            $this->values['btext']=$btext;


        $burl=$p->where('name','=','button_url')->first();
        if($burl)
            $this->values['burl']=$burl;


        return view('admin.site_config',$this->values);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save_banner(Request $request)
    {
        $this->validate($request,[
           'img'=>'image',
        ]);
        $img=$request->file('img');
        $ads_visible=$request->input('ads_visible');
        $ads_text=$request->input('ads_text');
        $title=$request->input('title');
        $text=$request->input('text');
        $btext=$request->input('btext');
        $burl=$request->input('burl');

        if($img!=null)
        {
            $ext=$img->getClientOriginalExtension();
            Image::make($img->getRealPath())->save('images/carousel/'.$img->getClientOriginalName());
            $p= Parametres::firstOrNew(['name'=>'banner_image']);
            $p->value='images/carousel/'.$img->getClientOriginalName();
            $p->save();

        }

        $p=Parametres::firstOrNew(['name'=>'show_ads']);
        if ($ads_visible)
        {
            $p->value=1;
        }
        else
        {
            $p->value=0;
        }
        $p->save();

        if($ads_text)
        {
            $p=Parametres::firstOrNew(['name'=>'ads_text']);
            $p->value=$ads_text;
            $p->save();

        }
        if($title)
        {
            $p=Parametres::firstOrNew(['name'=>'banner_title']);
            $p->value=$title;
            $p->save();

        }
        if($text)
        {
            $p=Parametres::firstOrNew(['name'=>'banner_text']);
            $p->value=$text;
            $p->save();

        }
        if($btext)
        {
            $p=Parametres::firstOrNew(['name'=>'button_text']);
            $p->value=$btext;
            $p->save();

        }
        if($burl)
        {
            $p=Parametres::firstOrNew(['name'=>'button_url']);
            $p->value=$burl;
            $p->save();

        }

        return redirect()->route('site_config')->withSuccess(['ok'=>'']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
