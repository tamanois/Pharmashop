<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Dosage;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Product;

class DosageController extends Controller
{
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage Dosages';
    }

    public function index($id)
    {
        $p=Product::find($id);
        $c=$p->dosages()->orderBy('description')->get();
        $this->values['Dosages']=$c;
        $this->values['product']=$p;
        return view('admin.dosage',$this->values);
    }


    public function store(Request $request,$id)
    {
        $this->validate($request,
            ['description'=>'required',
                'price'=>'required|numeric'
            ]);

        $description=$request->input('description');
        $price=$request->input('price');

        $c=new Dosage();
        $c->description=$description;
        $c->price=$price;
        $c->id_product=$id;
        $c->save();
        return redirect()->route('dosage_management',$id)->withSuccess(['ok'=>'']);
    }



    public function update(Request $request,$id_p)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'description'=>'required',
                'price'=>'required|numeric'
            ]);
        $id=$request->input('id');
        $description=$request->input('description');
        $price=$request->input('price');

        $c=Dosage::find($id);
        $c->description=$description;
        $c->price=$price;
        $c->save();
        return redirect()->route('dosage_management',$id_p)->withSuccess(['ok'=>'']);
    }


    public function destroy($id)
    {
        $id_p=Dosage::find($id)->id_product;
        Dosage::destroy($id);
        return redirect()->route('dosage_management',$id_p)->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');

        Dosage::destroy($idList);
        return response()->json('OK');



    }

}
