<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Faq;
use pharmashop\Faq_category;
use pharmashop\Http\Controllers\Controller;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage FAQs';
    }

    public function index()
    {

        $c=Faq::with('category')->orderBy('title')->get();
        $this->values['faqs']=$c;
        $this->values['categories']=Faq_category::orderBy('name')->get();
        return view('admin.faq',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title'=>'required',
                'content'=>'required',
                'category'=>'required',

            ]);
        $title=$request->input('title');
        $content=$request->input('content');
        $category=$request->input('category');


        $c=new Faq();

        $c->title=$title;
        $c->content=$content;
        $c->id_faq_category=$category;
        $c->save();
        return redirect()->route('faq_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'title'=>'required',
                'content'=>'required',
                'category'=>'required',


            ]);
        $id=$request->input('id');
        $title=$request->input('title');
        $content=$request->input('content');
        $category=$request->input('category');




        $c=Faq::find($id);
        $c->title=$title;
        $c->content=$content;
        $c->id_faq_category=$category;

        $c->save();
        return redirect()->route('faq_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::destroy($id);
        return redirect()->route('faq_management')->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');

        Faq::destroy($idList);
        return response()->json('OK');



    }
    public function getContent(Request $request)
    {
        $id=$request->input('id');

        $c=Faq::find($id);

        return response($c->content);



    }
}
