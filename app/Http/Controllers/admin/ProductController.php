<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use pharmashop\Category;
use pharmashop\Dosage;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Mail\registerNotification;
use pharmashop\Product;
use pharmashop\User;

class ProductController extends Controller
{
    protected $values=[];
    protected   $img_size=200;

    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage products';
    }

    public function index()
    {

        $cat=Category::orderBy('name')->get();
        $c=Product::with('category')->orderBy('name')->get();
        $this->values['products']=$c;
        $this->values['categories']=$cat;

        return view('admin.product',$this->values);
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'img'=>'image|max:2048',
                'category'=>'required|numeric',



            ]);


        $name=$request->input('name');
        $generic_name=$request->input('generic_name');
        $description=$request->input('description');
        $img=$request->file('img');
        $id_cat=$request->input('category');




        $c=new Product();

        $c->name=$name;
        $c->generic_name=$generic_name;
        $c->description=$description;
        $c->id_category=$id_cat;

        $c->save();
        if($img!=null){
            $image=Image::make($img->getRealPath());
            $image=$image->resize($this->img_size,$this->img_size)->encode('jpg');
            $image->save('images/products/'.$c->id.'.jpg');
            $c->imgurl='images/products/'.$c->id.'.jpg';
            $c->save();
        }


        return redirect()->route('product_management')->withSuccess(['ok'=>'']);
    }



    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'name'=>'required',
                'img'=>'image|max:2048',
                'category'=>'required|numeric',



            ]);


        $id=$request->input('id');
        $name=$request->input('name');
        $generic_name=$request->input('generic_name');
        $description=$request->input('description');
        $img=$request->file('img');
        $id_cat=$request->input('category');




        $c=Product::find($id);

        $c->name=$name;
        $c->generic_name=$generic_name;
        $c->description=$description;
        $c->id_category=$id_cat;
        if($img!=null){
            $image=Image::make($img->getRealPath());
            $image=$image->resize($this->img_size,$this->img_size)->encode('jpg');
            $image->save('images/products/'.$c->id.'.jpg');
            $c->imgurl='images/products/'.$c->id.'.jpg';
        }

        $c->save();



        return redirect()->route('product_management')->withSuccess(['ok'=>'']);
    }


    public function destroy($id)
    {
        Product::destroy($id);
        return redirect()->route('product_management')->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');
        $p=Product::whereIn('id',$idList)->get();
        $dosages=Dosage::whereIn('id_product',$idList)->get(['id']);

        $darray=$dosages->toArray();
        Dosage::destroy($darray);
        foreach ($p as $pro)
        {
            if($pro->imgurl)
                @unlink($pro->imgurl);

        }
        Product::destroy($idList);
        return response()->json('OK');



    }

    public function updateFeature(Request $request)
    {
        $id=$request->input('id');
        $option=$request->input('option');
        $c=Product::find($id);
        if ($option==1)
        {
            $c->feature=1;
        }
        else
        {
            $c->feature=0;
        }
        $c->save();

        return response($c->id);



    }

    public function getContent(Request $request)
    {
        $id=$request->input('id');
        $c=Product::find($id);

        return response($c->description);



    }

}
