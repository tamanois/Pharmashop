<?php

namespace pharmashop\Http\Controllers\admin;

use Illuminate\Http\Request;
use pharmashop\Country;
use pharmashop\Http\Controllers\Controller;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct()
    {
        $this->values['big_title']='Basic configuration';

        $this->values['title']='Manage countries';
    }

    public function index()
    {
        $c=Country::orderBy('name')->get();
        $this->values['countries']=$c;
        return view('admin.country',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name'=>'required|unique:countries',
                'ship'=>'required|numeric',

            ]);
        $name=$request->input('name');
        $ship=$request->input('ship');
        $price=$request->input('price');


        $c=new Country();
        if ($price)
            $c->ship_price=$price;
        else
            $c->ship_price=0;

        $c->name=$name;
        $c->ship=$ship;
        $c->save();
        return redirect()->route('country_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id'=>'required|numeric',
                'name'=>'required|unique:categories',
                                'ship'=>'required|numeric',

            ]);
        $id=$request->input('id');
        $name=$request->input('name');
        $ship=$request->input('ship');
        $price=$request->input('price');



        $c=Country::find($id);
        $c->name=$name;
        $c->ship=$ship;
        if ($price)
            $c->ship_price=$price;

        $c->save();
        return redirect()->route('country_management')->withSuccess(['ok'=>'']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::destroy($id);
        return redirect()->route('country_management')->withSuccess(['ok'=>'']);


    }
    public function destroys(Request $request)
    {
        $idList=$request->input('check');

        Country::destroy($idList);
        return response()->json('OK');



    }
}
