<?php

namespace pharmashop\Http\Controllers\Auth;

use Illuminate\Http\Request;
use pharmashop\Notification;
use pharmashop\User;
use pharmashop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function check_mail(Request $request)
    {
        $this->validate($request,[
            "email"=>'required|email|unique:users'
        ]);
        return view('auth.register')->with(['email'=>$request->input('email')]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \pharmashop\User
     */
    protected function create(array $data)
    {
        if (isset($data['forname'])){
            $n=new Notification(['message'=>'<strong>'.$data['forname'].' '.$data['name'].'</strong> has just registered to the platform','type'=>10]);
            $n->save();
            return User::create([
            'name' => $data['name'],
            'forname'=>$data['forname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);}
        else
        {
            $n=new Notification(['message'=>'<strong>'.$data['name'].'</strong> has just registered to the platform','type'=>10]);
            $n->save();
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
        }
    }
}
