<?php

namespace pharmashop\Http\Controllers;

use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use pharmashop\Category;
use pharmashop\DataCheck;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Mail\contactMail;
use pharmashop\Parametres;
use pharmashop\User;
use Symfony\Component\VarDumper\Cloner\Data;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];


    public function index(Request $request)

    {
        $lp=DataCheck::getLastProducts();
        $fp=DataCheck::getFeaturedProducts();

        $p=Parametres::all();
        $imgurl=$p->where('name','=','banner_image')->first();
        if($imgurl)
            $this->values['img']=$imgurl;

        $ads_visible=$p->where('name','=','show_ads')->first();
        if($ads_visible)
            $this->values['ads_visible']=$ads_visible;

        $ads_text=$p->where('name','=','ads_text')->first();
        if($ads_text)
            $this->values['ads_text']=$ads_text;

        $title=$p->where('name','=','banner_title')->first();
        if($title)
            $this->values['batitle']=$title;

        $text=$p->where('name','=','banner_text')->first();
        if($text)
            $this->values['batext']=$text;

        $btext=$p->where('name','=','button_text')->first();
        if($btext)
            $this->values['btext']=$btext;


        $burl=$p->where('name','=','button_url')->first();
        if($burl)
            $this->values['burl']=$burl;


        return view('home',$this->values)->with(['alpha_searchable'=>true,'page_title'=>'Lastest products','products'=>$lp,'dosages'=>$fp,'active_home'=>true,'active_home_page'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $type=$request->input('type');
        $val=$request->input('value');
        if($type=='category'){
            $c=Category::find($val);
            $p=DataCheck::searchProductByCategory($val);
            return view('product_search')->with(['page_title'=>$c->name,'title'=>'Get '.$c->name,'products'=>$p,'active_products'=>true]);

        }elseif ($type=='name'){
            $p=DataCheck::searchProductByName($val);
            return view('product_search')->with(['page_title'=>'Search results for <<'.$val.'>>','title'=>$val,'products'=>$p,'active_products'=>true]);

        }
        else
        {
            $p=DataCheck::getAllProducts();
            return view('product_search')->with(['page_title'=>'All products','title'=>'All products','products'=>$p,'active_products'=>true]);

        }
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function contact()
    {
        $values['title']='Contact';
        $values['active_contact']=true;

        return view('contact',$values);

    }
    public function send_mail(Request $request)
    {
        $myValidator=\Validator::make($request->all(),[
           'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
            'message'=>'required|min:20',

        ]);
        if($myValidator->fails()){
            $errors=$myValidator->errors();
            $errors=$errors->all();
            return response()->json(['errors'=>$errors]);

        }
        $userArr['name']=$request->input('name');
        $userArr['email']=$request->input('email');
        $userArr['subject']=$request->input('subject');
        $userArr['message']=$request->input('message');

        $admin=User::where('level','<',2)->get();

        Mail::to($admin)->queue(new contactMail($userArr));


        return response('OK');


    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getToken()
    {
        return response()->json(['token'=>csrf_token()]);
    }
}
