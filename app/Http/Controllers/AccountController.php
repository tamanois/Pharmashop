<?php

namespace pharmashop\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use pharmashop\Billing;
use pharmashop\Country;
use pharmashop\Order;
use pharmashop\Shipping;
use pharmashop\User;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values;
    protected $token;
    protected $pServer;
    function __construct()
    {
        $this->values=[];
        $this->token=env('CRYPTO_API_TOKEN');
        $this->pServer=env('PAYMENT_SERVER');
    }

    public function index()
    {

        $this->values['title']='My account';
        $u=\Auth::user();
        $this->values['user']=$u;
        $this->values['infos']=true;
        $this->values['account_title']='Personnals informations';

        return view('account-infos',$this->values);
    }

    public function index_addresses()
    {
        $this->values['title']='My account';
        $u=\Auth::user();

        $sa=Shipping::with('country')->where('id_user','=',$u->id)->first();
        $ba=Billing::with('country')->where('id_user','=',$u->id)->first();

        $c=Country::orderBy('name')->get();
        $c2=$c->where('ship','=',1)->all();
        $this->values['shipping_address']=$sa;
        $this->values['billing_address']=$ba;
        $this->values['countries']=$c;
        $this->values['scountries']=$c2;
        $this->values['user']=$u;
        $this->values['address']=true;
        $this->values['account_title']='Addresses';


        return view('account-addresses',$this->values);
    }

    public function index_orders()
    {
        $this->values['title']='My orders';
        $u=\Auth::user();

        $o=Order::where('id_user','=',$u->id)->orWhere('email','=',$u->email)->orderBy('created_at','desc')->get();


        $this->values['user']=$u;
        $this->values['orders']=$o;

        $this->values['order']=true;
        $this->values['account_title']='My orders';




        return view('account-orders',$this->values);
    }

    public function get_order($id)
    {
        $this->values['title']='Order #'.sprintf('%08d',$id);
        $u=\Auth::user();


        $o=Order::with(['order_items.dosage.product','billing.country','shipping.country'])->find($id);
        if($o->id_user=$u->id || $o->email==$u->email || $u->level<2)
        {
            $this->values['user']=$u;
            $this->values['order']=$o;


            return view('order',$this->values);
        }

        return redirect()->route('home');


    }

    function update_state(Request $request)
    {


        $id = $request->input('id');
        $u = User::find($id);
        $OrdersStates=[];
        $o = Order::where('id_user', '=', $u->id)
            ->orWhere('email', '=', $u->email)
            ->orderBy('created_at', 'desc')->get()
            ->where('status', '=', 0)
            ->where('payment_url','<>','card_payment');

        $i=0;

        foreach ($o as $order) {

            @$sCheckoutData = json_decode($this->checkOrderOnServer($order->server_order_id, $order->currency));

            if (property_exists($sCheckoutData, 'status')) {

                $status = $sCheckoutData->status;

                if ($status == 'canceled' || $status == 'expired')
                    $order->status = 1;
                else if ($status == 'invalid')
                    $order->status = 2;
                else if ($status == 'refunded')
                    $order->status = 3;
                else if ($status == 'confirming')
                    $order->status = 4;
                else if ($status == 'paid')
                    $order->status = 5;


            if($order->status!=0)
            {
                $OrdersStates[$i]=new OrderState($order->id,$status);
                $i++;
                $order->save();
            }
            }
        }
        return response()->json($OrdersStates);
    }

    protected function checkOrderOnServer($orderId,$currency)
    {
        $url=$this->pServer. "/v2/orders/$orderId/checkout";

        $curl=curl_init();
        $headers   = array();
        $headers[] = 'Authorization: Token ' . $this->token;
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "pay_currency=$currency");

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($curl);
        curl_close($curl);
        return $server_output;
    }

    public function show_bill(Request $request)
    {
        $id=$request->input('id');
        $o=Order::with(['order_items.dosage.product','billing.country','shipping.country'])->find($id);
        if($o->status==5){
            if($o->id_user!=0) {
                $u = User::find($o->id_user);
                $this->values['user'] = $u;
            }
            $this->values['order']=$o;
            $pdf = PDF::loadView('order_bill',$this->values);
            return $pdf->stream('Bill-B'.sprintf('%08d',$id).'.pdf');
        }
        return redirect()->to(\URL::previous());



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function update_infos(Request $request)
    {



        $name=$request->input('name');
        $forname=$request->input('forname');
        $email=$request->input('email');
        $u=\Auth::user();
        if($name)
            $u->name=$name;
        if($forname)
            $u->forname=$forname;
        if($email){
            $this->validate($request,[
                'email'=>'email'
            ]);
            $u2=User::whereEmail($email)->first();
            if($u2)
            {
                return redirect()->route('account')->withErrors(['email'=>'This email is already taken']);
            }
            $u->email=$email;

        }
        $u->save();

        return redirect()->route('account');
    }


    public function update_billing(Request $request)
    {

        $country=$request->input('country');
        $city=$request->input('city');
        $address=$request->input('address');
        $postal_code=$request->input('postal_code');

        $u=\Auth::user();
        $ba=Billing::whereIdUser($u->id)->first();
        if(!$ba){
            $ba=new Billing();
            $this->validate($request,[
               'country'=>'required|numeric',
                'city'=>'required',
                'address'=>'required',
                'postal_code'=>'required',

            ]);
        }
        if($country)
            $ba->id_country=$country;
        if($city)
            $ba->city=$city;
        if($address)
            $ba->address=$address;
        if($postal_code)
            $ba->postal_code=$postal_code;

        $ba->id_user=$u->id;

        $ba->save();


        return redirect()->route('account_addresses');
    }

    public function update_shipping(Request $request)
    {

        $country=$request->input('country');
        $city=$request->input('city');
        $address=$request->input('address');
        $postal_code=$request->input('postal_code');
        $phone=$request->input('phone');


        $u=\Auth::user();
        $sa=Shipping::whereIdUser($u->id)->first();
        if(!$sa){
            $sa=new Shipping();
            $this->validate($request,[
                'country'=>'required|numeric',
                'city'=>'required',
                'address'=>'required',
                'postal_code'=>'required',
                'phone'=>'required',


            ]);
        }
        if($country)
            $sa->id_country=$country;
        if($city)
            $sa->city=$city;
        if($address)
            $sa->address=$address;
        if($postal_code)
            $sa->postal_code=$postal_code;
        if($phone)
            $sa->phone=$phone;

        $sa->id_user=$u->id;
        $sa->save();


        return redirect()->route('account_addresses');
    }



    public function update_password(Request $request)
    {

        $this->validate($request,[
            'old_password'=>'required',
            'new_password'=>'required|confirmed|min:6'
        ]);

        $old=$request->input('old_password');
        $new=$request->input('new_password');
        $u=\Auth::user();


        if (!\Hash::check($old,$u->password)){
            return redirect()->route('account')->withErrors(['bad_password'=>'The old password is incorrect']);
        }
        $u->password=bcrypt($new);
        $u->save();

        return redirect()->route('account');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

class OrderState{
    public $id;
    public $state;

    /**
     * OrderState constructor.
     * @param $id
     * @param $state
     */
    public function __construct($id, $state)
    {
        $this->id = $id;
        $this->state = $state;
    }

}
