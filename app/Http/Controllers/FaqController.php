<?php

namespace pharmashop\Http\Controllers;

use Illuminate\Http\Request;
use pharmashop\Faq;
use pharmashop\Faq_category;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Policy;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values;

    function __construct()
    {
        $this->values=[];
    }

    public function index(Request $request){
        $this->values['title']='HELP & FAQ';
        $this->values['active_faq']=true;
        $category=$request->input('category');
        if($category!=null){
            $cat=Faq_category::find($category);
            $f=Faq::whereIdFaqCategory($category)->orderBy('title')->get();

        }
        else{
            $cat=new Faq_category();
            $cat->name='All questions';
            $f=Faq::orderBy('title')->get();

        }
        $fq=Faq_category::orderBy('name')->get();
        $this->values['faqs']=$f;
        $this->values['cat']=$cat;

        $this->values['faq_categories']=$fq;

        return view('faq',$this->values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_policy($id)
    {
        $p=Policy::find($id);
        $this->values['title']=$p->title;
        $this->values['policy']=$p;

        return view('policy',$this->values);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
