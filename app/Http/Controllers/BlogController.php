<?php

namespace pharmashop\Http\Controllers;

use Illuminate\Http\Request;
use pharmashop\Article;
use pharmashop\Http\Controllers\Controller;

class BlogController extends Controller
{
    protected $values;

    function __construct()
    {
        $this->values=[];
    }

    public function index(){
        $this->values['title']='Blog';
        $this->values['active_blog']=true;
        $a=Article::with('author')->orderBy('created_at','desc')->get();
        $this->values['articles']=$a;
        return view('blog',$this->values);
    }

    public function show($id)
    {
        $this->values['active_blog']=true;
        $a=Article::find($id);
        $this->values['title']=$a->title;
        $this->values['article']=$a;

        return view('blog-article',$this->values);



    }
}
