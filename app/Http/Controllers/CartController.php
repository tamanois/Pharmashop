<?php

namespace pharmashop\Http\Controllers;

use Braintree_Gateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use PDF;
use pharmashop\Billing;
use pharmashop\Cart;
use pharmashop\Cart_product;
use pharmashop\Country;
use pharmashop\DataCheck;
use pharmashop\Http\Controllers\Controller;
use pharmashop\Mail\orderConfirmMail;
use pharmashop\Notification;
use pharmashop\Order;
use pharmashop\OrderItem;
use pharmashop\Shipping;
use pharmashop\User;
use Psy\Util\Json;
require_once resource_path().'\views\perso\braintree-php\lib\Braintree.php';


class CartController extends Controller
{

    protected $values;
    protected $token;
    protected $pServer;
    protected $gateway;

    function __construct()
    {
        $this->values=[];
        $this->token=env('CRYPTO_API_TOKEN');
        $this->pServer=env('PAYMENT_SERVER');
        $this->gateway = new Braintree_Gateway([
            'environment' => 'sandbox',
            'merchantId' => env('BRAINTREE_MERCHANT_ID'),
            'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
            'privateKey' => env('BRAINTREE_PRIVATE_KEY')
        ]);
    }

    public function show()
    {

        $this->values['page_title']='Shopping cart';
        $this->values['title']='Shopping cart - '.trans('main.site_name');

        $id_cart=Cookie::get('cart');
        $co=Country::orderBy('name')->get();
        $this->values['countries']=$co;
        if($id_cart){
            $cart=Cart::find($id_cart);
            if($cart->id_country!=0)
            {
                $shipCountry=Country::find($cart->id_country);
                $this->values['shipCountry']=$shipCountry;
            }
            $cp=Cart_product::with('dosage.product')->where('id_cart','=',$id_cart)->get();
            $this->values['cart_products']=$cp;
            return view('cart',$this->values);

        }
        else
        {
            $c = new Cart();
            if (\Auth::check())
                $c->id_user = \Auth::user()->id;
            $c->save();
            $cookie = \cookie('cart', $c->id, 10080);
            $cp=Cart_product::with('dosage.product')->where('id_cart','=',$id_cart)->get();
            $this->values['cart_products']=$cp;
            return view('cart',$this->values)->withCookie($cookie);


        }


    }

    public function getDosage(Request $request)
    {
        $id=$request->input('id');
        $d=DataCheck::getDosage($id);
        $d->product->imgurl=asset($d->product->imgurl);
        return response()->json($d);

    }

    public function update(Request $request)
    {

        $id = $request->input('id');
        $quantity = $request->input('quantity');

        $id_cart = Cookie::get('cart');
        if ($id_cart) {
            $cp = Cart_product::firstOrNew(['id_cart' => $id_cart, 'id_dosage' => $id]);
            $cp->quantity = $quantity;
            $cp->save();
            $c = Cart::with(['cart_products.dosage'])->find($id_cart);
            $camount = 0;
            $n = 0;

            foreach ($c->cart_products as $cp) {
                $camount += ($cp->dosage->price * $cp->quantity);
                $n++;
            }
            $camount = round($camount, 2);
            $ccount = $n;
            $data['camount'] = $camount;
            $data['ccount'] = $ccount;
            $data['cart'] = $c;

            return response()->json([$data]);


        } else {
            $c = new Cart();
            if (\Auth::check())
                $c->id_user = \Auth::user()->id;
            $c->save();
            $cp = new Cart_product();
            $cp->id_cart = $c->id;
            $cp->id_dosage = $id;
            $cp->quantity = $quantity;
            $cp->save();
            $c = Cart::with(['cart_products.dosage'])->find($c->id);
            $camount = 0;
            $n = 0;
            foreach ($c->cart_products as $cp) {
                $camount += ($cp->dosage->price * $cp->quantity);
                $n++;
            }
            $camount = round($camount, 2);
            $ccount = $n;
            $data['camount'] = $camount;
            $data['ccount'] = $ccount;
            $data['cart'] = $c;

            $cookie = \cookie('cart', $c->id, 10080);
            return response()->json([$data])->withCookie($cookie);

        }
    }

        public function refresh(){
            $id_cart=Cookie::get('cart');
            if($id_cart){

                $c=Cart::with(['cart_products.dosage'])->find($id_cart);
                $camount=0;
                $n=0;

                foreach ($c->cart_products as $cp){
                    $camount+=($cp->dosage->price*$cp->quantity);
                    $n++;
                }
                $camount=round($camount,2);
                $ccount=$n;
                $data['camount']=$camount;
                $data['ccount']=$ccount;
                $data['cart']=$c;

                return response()->json([$data]);


            }
            $c = new Cart();
            if (\Auth::check())
                $c->id_user = \Auth::user()->id;
            $c->save();
            $cookie = \cookie('cart', $c->id, 10080);
            return response()->json()->withCookie($cookie);
        }

        public  function remove_product(Request $request){
            $id=$request->input('id');
            $id_cart=Cart_product::find($id)->id_cart;
            Cart_product::destroy([$id]);
            $cp=Cart_product::whereIdCart($id_cart)->get();
            if($cp->count()==0){
                return response(0);

            }
            return response('OK');
        }


        public function confirm(Request $request){
            $this->validate($request,
                [
                   'ship_country'=>'required|numeric',
                ]);


            $shipCountryId=$request->input('ship_country');
            $idCartProducts=$request->input('idCartProducts');
            $quantities=$request->input('quantities');

            $id_cart=Cookie::get('cart');
            $cart=Cart::find($id_cart);
            $cart->id_country=$shipCountryId;
            $cart->save();

            $i=0;
            foreach ($idCartProducts as $icp)
            {
                $cp=Cart_product::find($icp);
                $cp->quantity=$quantities[$i];
                $cp->save();
                $i++;
            }

            return redirect()->route('cart_address');

        }

        public function setPaymentAddresses(){
            $this->values['page_title']='Payment adresses';
            $this->values['title']='Payment adresses - '.trans('main.site_name');

            $id_cart=Cookie::get('cart');
            $cart=Cart::find($id_cart);
            $country=Country::find($cart->id_country);
            $cp=Cart_product::with('dosage')->where('id_cart','=',$id_cart)->get();
            $price=0;
            $total=0;
            foreach ($cp as $product)
            {
                $price+=$product->dosage->price*$product->quantity;
            }

            $total=$price+$country->ship_price;

            $bi=null;$shi=null;

            if(\Auth::check())
            {
                $u=\Auth::user();
                $bi=Billing::with('country')->whereIdUser($u->id)->first();
                $shi=Shipping::with('country')->whereIdUser($u->id)->first();
                if($bi==null)
                {
                    if($cart->id_billing_address!=0)
                        $bi=Billing::with('country')->where('id','=',$cart->id_billing_address)->first();
                }
                if($shi==null)
                {
                    if($cart->id_shipping_address!=0)
                        $shi=Shipping::with('country')->where('id','=',$cart->id_shipping_address)->first();
                }

            }else
            {
                if($cart->id_billing_address!=0)
                $bi=Billing::with('country')->where('id','=',$cart->id_billing_address)->first();

                if($cart->id_shipping_address!=0)
                $shi=Shipping::with('country')->where('id','=',$cart->id_shipping_address)->first();

            }

            $c=Country::orderBy('name')->get();
            $this->values['countries']=$c;

            $this->values['nbItems']=$cp->count();
            $this->values['ship_country']=$country;
            $this->values['price']=$price;

            if ($cart->email)
                $this->values['email']=$cart->email;

            if ($shi)
            $this->values['shipping_address']=$shi;

            if($bi)
            $this->values['billing_address']=$bi;

            return view('cart_address',$this->values);
        }
        public function proceedPayment(Request $request)
        {
            $rules=[
              'shipping_country'=>'required|numeric',
                'shipping_city'=>'required',
                'shipping_postal_code'=>'required',
                'shipping_address'=>'required',
                'shipping_phone_number'=>'required',

                'billing_country'=>'required',
                'billing_city'=>'required',
                'billing_address'=>'required',
                'billing_postal_code'=>'required',

            ];

            if(!\Auth::check())
                    $rules['email']='required|email';

            $this->validate($request,$rules);

            $id_cart=Cookie::get('cart');
            $cart=Cart::find($id_cart);
            if(!\Auth::check())
                $cart->email=$request->input('email');
            $cart->id_country=$request->input('shipping_country');

            if($cart->id_shipping_address!=0)
                $shi=$cart->shipping;
            else
                $shi=new Shipping();

            if($cart->id_billing_address!=0)
                $bi=$cart->billing;
            else
                $bi=new Billing();

            $bi->id_country=$request->input('billing_country');
            $bi->address=$request->input('billing_address');
            $bi->city=$request->input('billing_city');
            $bi->postal_code=$request->input('billing_postal_code');
            $bi->id_user=0;

            $shi->id_country=$request->input('shipping_country');
            $shi->city=$request->input('shipping_city');
            $shi->postal_code=$request->input('shipping_postal_code');
            $shi->address=$request->input('shipping_address');
            $shi->phone=$request->input('shipping_phone_number');
            $shi->id_user=0;

            $bi->save();$shi->save();

            $cart->id_billing_address=$bi->id;
            $cart->id_shipping_address=$shi->id;
            $cart->save();
            return redirect()->route('choose_pay_option');
        }

        public function payOptions(){
            $this->values['page_title']='Choose Payment method';
            $this->values['title']='Payment - '.trans('main.site_name');
            $id_cart=Cookie::get('cart');
            if ($id_cart)
            {
                $cart=Cart::find($id_cart);
                $country=Country::find($cart->id_country);
                $cp=Cart_product::with('dosage')->where('id_cart','=',$id_cart)->get();
                $price=0;
                $total=0;
                foreach ($cp as $product)
                {
                    $price+=$product->dosage->price*$product->quantity;
                }

                $o=Order::where('id_cart','=',$id_cart)->first();
                if($o)
                {
                    $this->values['order']=$o;
                }
                $total=$price+$country->ship_price;

                $this->values['total']=$total;
                $this->values['cart']=$cart;

                return view('proceed_payment',$this->values);

            }
          return redirect()->route('home');
        }

    public function proceedCrypto($currency){
        $id_cart=Cookie::get('cart');
        if ($id_cart)
        {

            $cart=Cart::find($id_cart);
            $country=Country::find($cart->id_country);
            $cp=Cart_product::with('dosage')->where('id_cart','=',$id_cart)->get();
            $price=0;
            $total=0;
            foreach ($cp as $product)
            {
                $price+=$product->dosage->price*$product->quantity;
            }


            $total=$price+$country->ship_price;

            //verifie si la commande a deja ete faite
            $old_order_id=$cart->server_order_id;
            if ($old_order_id!=0){

                @$sCheckoutData=json_decode($this->checkOrderOnServer($old_order_id,$currency));

                if(property_exists($sCheckoutData,'status'))
                {
                    if($sCheckoutData->status=='pending' || $sCheckoutData->status=='confirming'){
                        $d=date('H:i:s');
                        return response()->json(['payment_data'=>$sCheckoutData,'now'=>$d]);
                    }


                }
            }

            //Creating order
            $url=$this->pServer. '/v2/orders/';

            $curl=curl_init();
            $headers   = array();
            $headers[] = 'Authorization: Token ' . $this->token;
            curl_setopt($curl, CURLOPT_URL,$url);
            curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS,
                "order_id=$id_cart&price_amount=$total&price_currency=EUR&receive_currency=DO_NOT_CONVERT&title=Cart #$id_cart");

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($curl);
            curl_close($curl);

            @$serverData=json_decode($server_output);


            //checking order
            if(property_exists($serverData,'id')){
                
                @$sCheckoutData=json_decode($this->checkOrderOnServer($serverData->id,$currency));

                if(property_exists($sCheckoutData,'status'))
                {
                    $cart->server_order_id=$sCheckoutData->id;
                    $cart->save();
                    //creating order
                    $this->createNewOrder($cart,$sCheckoutData,$cp);

                    $d=date('H:i:s');
                    return response()->json(['payment_data'=>$sCheckoutData,'now'=>$d]);

                }
        }
            return response('',500);


        }
        return response('empty_cart');

    }

    public function checkOrder(Request $request,$currency)
    {
        $orderId=$request->input('id');
        if ($orderId){


            @$sCheckoutData=json_decode($this->checkOrderOnServer($orderId,$currency));
            if(property_exists($sCheckoutData,'status'))
            {
                $d=date('H:i:s');
                if($sCheckoutData->status=='paid')
                {
                    $o=Order::where('server_order_id','=',$sCheckoutData->id)->first();
                    return response()->json(['payment_data'=>$sCheckoutData,'now'=>$d,'id'=>$o->id]);

                }
                return response()->json(['payment_data'=>$sCheckoutData,'now'=>$d]);

            }
        }
        return response('',500);

    }

     public function confirmPayment(Request $request){

     }


     protected function checkOrderOnServer($orderId,$currency)
     {
         $url=$this->pServer. "/v2/orders/$orderId/checkout";

         $curl=curl_init();
         $headers   = array();
         $headers[] = 'Authorization: Token ' . $this->token;
         curl_setopt($curl, CURLOPT_URL,$url);
         curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($curl, CURLOPT_POST, 1);
         curl_setopt($curl, CURLOPT_POSTFIELDS,
             "pay_currency=$currency");

         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

         $server_output = curl_exec($curl);
         curl_close($curl);
         return $server_output;
     }



     protected function createNewOrder($cart,$sCheckoutData,$cp){
         $o=new  Order();
         if(\Auth::check())
             $o->id_user=\Auth::user()->id;
         else
             $o->email=$cart->email;
         $o->server_order_id=$sCheckoutData->id;
         $o->id_shipping_address=$cart->id_shipping_address;
         $o->id_billing_address=$cart->id_billing_address;
         $o->currency=$sCheckoutData->pay_currency;
         $o->pay_amount=$sCheckoutData->pay_amount;
         $o->amount=$sCheckoutData->price_amount;
         $o->payment_url=$sCheckoutData->payment_url;
         $o->id_cart=$cart->id;
         $o->status=0;
         $o->save();

         foreach ($cp as $p)
         {
             $oi=new OrderItem();
             $oi->id_order=$o->id;
             $oi->id_dosage=$p->id_dosage;
             $oi->quantity=$p->quantity;
             $oi->price=$p->dosage->price;
             $oi->save();
         }
     }



     public function updateOrderState(Request $request)
     {


         $currency=$request->input('currency');
         $orderId=$request->input('id');
         if ($orderId){



             @$sCheckoutData=json_decode($this->checkOrderOnServer($orderId,$currency));
             if($sCheckoutData=='{}' || $sCheckoutData=='{false}')
                 @$sCheckoutData=json_decode($this->checkOrderOnServer($orderId,$currency));

             if(property_exists($sCheckoutData,'status'))
             {
                 //$d=date('H:i:s');

                 $status=$sCheckoutData->status;
                 $o=Order::where('server_order_id','=',$orderId)->first();

                 if ($status=='canceled' || $status=='expired')
                     $o->status=1;
                 else if ($status=='invalid')
                     $o->status=2;
                 else if ($status=='refunded')
                     $o->status=3;
                 else if ($status=='confirming')
                     $o->status=4;
                 else if ($status=='paid')
                     $o->status=5;


                 $o->save();

                 if($status=='paid'){
                     $cart=Cart::find($o->id_cart);
                     if(\Auth::check())
                     {
                         $u=\Auth::user();
                         $name=$u->forname.' '.$u->name;
                     }
                     else
                         $name=$cart->email;
                     $cp=Cart_product::where('id_cart','=',$cart->id)->get();
                     $ids=$cp->getQueueableIds();

                     Cart_product::destroy($ids);
                     $cart->delete();
                     $o->id_cart=0;
                     $o->save();

                     (New Notification(['type'=>0,'Message'=>"A new Order has been made by <strong>$name</strong>. The amount  <strong class='text-primary'>$o->amount</strong> ".trans('main.currency')."  is paid and confirmed. Please check this as fast as you can"]))->save();



                   $cook= Cookie::forget('cart');
                    return response()->json(['id'=>$o->id])->withCookie($cook);
                 }




                 return response('OK');


             }
         }
         return response('',500);
     }


     public function initializeCardPayment()
     {

         $client_token = $this->gateway->clientToken()->generate();

         return response()->json(['token'=>$client_token]);

     }
     public function proceedCardPayment(Request $request)
     {
         $id_cart = Cookie::get('cart');
         $total = 0;
         if ($id_cart) {

             $cart = Cart::find($id_cart);
             $country = Country::find($cart->id_country);
             $cp = Cart_product::with('dosage')->where('id_cart', '=', $id_cart)->get();
             $price = 0;
             $total = 0;
             foreach ($cp as $product) {
                 $price += $product->dosage->price * $product->quantity;
             }


             $total = $price + $country->ship_price;


             $o=new  Order();
             if(\Auth::check()){
                 $u=Auth::user();
                 $name=$u->forname.' '.$u->name;
                 $o->id_user=\Auth::user()->id;

             }
             else{
                 $o->email=$cart->email;
                    $name=$cart->email;
             }
             //$o->server_order_id=$sCheckoutData->id;
             $o->id_shipping_address=$cart->id_shipping_address;
             $o->id_billing_address=$cart->id_billing_address;
             $o->currency=trans('main.currency_abb');
             //$o->pay_amount=$sCheckoutData->pay_amount;
             $o->amount=round($total,2);
             $o->payment_url='card_payment';
             $o->id_cart=$cart->id;
             $o->status=2;
             $o->save();

             foreach ($cp as $p)
             {
                 $oi=new OrderItem();
                 $oi->id_order=$o->id;
                 $oi->id_dosage=$p->id_dosage;
                 $oi->quantity=$p->quantity;
                 $oi->price=$p->dosage->price;
                 $oi->save();
             }


         $nonce = $request->input('paymentMethodNonce');


         $result = $this->gateway->transaction()->sale([
             'amount' => round($total,2),
             'paymentMethodNonce' => $nonce,
             'options' => [
                 'submitForSettlement' => true,
             ]
         ]);


         $cook = Cookie::forget('cart');

         if ($result->success) {
             $o->status=5;
             $o->server_order_id=$result->transaction->id;

             $o->save();
             (New Notification(['type'=>0,'Message'=>"A new Order has been made by <strong>$name</strong>. The amount  <strong class='text-primary'>$o->amount</strong> ".trans('main.currency')."  is paid and confirmed. Please check this as fast as you can"]))->save();

             return response()->json(['success' => $result->transaction->id,'id' => $o->id])->withCookie($cook);

         } else if ($result->transaction) {
             $o->status=2;
             $o->save();
             return response()->json([$result->transaction->processorResponseCode, $result->transaction->processorResponseText], 500);

         } else {
             $o->status=2;
             $o->save();
             return response()->json([$result->errors->deepAll()], 500);

         }
     }


     }

     function orderEmailNotification(Request $request){
         $id=$request->input('id');

         $o=Order::with(['order_items.dosage.product','billing.country','shipping.country'])->find($id);

         if($o->status==5){
             if($o->id_user!=0) {
                 $u = User::find($o->id_user);
             }else
             {
                 $u=new User();
                 $u->email=$o->email;
             }
             $this->values['order']=$o;
             PDF::loadView('order_bill',$this->values)->save("bills/$o->id.pdf");
             $pdf= "bills/$o->id.pdf";

             \Mail::to($u)->queue(new orderConfirmMail($pdf));
         }
     }



}
