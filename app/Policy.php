<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Policy
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Policy extends Model
{
    //
}
