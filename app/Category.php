<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Category
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    public function products()
    {
        return $this->hasMany(Product::class,'id_category');
    }
}
