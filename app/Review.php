<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Review
 *
 * @property int $id
 * @property int $id_user
 * @property float $note
 * @property string $comment
 * @property int $id_product
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Product $product
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Review extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class,'id_user');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'id_product');
    }
}
