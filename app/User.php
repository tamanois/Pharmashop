<?php

namespace pharmashop;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * pharmashop\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $forname
 * @property string $email
 * @property string $password
 * @property string $level
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Article[] $articles
 * @property-read \pharmashop\Billing $billing
 * @property-read \pharmashop\Cart $carts
 * @property-read \pharmashop\Review $reviews
 * @property-read \pharmashop\Shipping $shipping
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereForname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \pharmashop\Order $orders
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];

    public function billing()
    {
        return $this->belongsTo(Billing::class,'id_billing');
    }
    public function shipping()
    {
        return $this->belongsTo(Shipping::class,'id_shipping');
    }

    public function articles()
    {
        return $this->hasMany(Article::class,'id_user');
    }
    public function carts()
    {
        return $this->hasOne(Cart::class,'id_user');
    }
    public function orders()
    {
        return $this->hasOne(Order::class,'id_user');
    }
    public function reviews()
    {
        return $this->hasOne(Review::class,'id_user');
    }
}
