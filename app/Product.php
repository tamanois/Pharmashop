<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Product
 *
 * @property int $id
 * @property string $name
 * @property string|null $generic_name
 * @property string|null $description
 * @property string|null $imgurl
 * @property int|null $id_category
 * @property int $feature
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Dosage[] $dosages
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereFeature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereGenericName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereIdCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereImgurl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class,'id_category');
    }

    public function dosages()
    {
        return $this->hasMany(Dosage::class,'id_product');
    }
}
