<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\OrderItem
 *
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class,'id_order');
    }
    public function dosage()
    {
        return $this->belongsTo(Dosage::class,'id_dosage');
    }
}
