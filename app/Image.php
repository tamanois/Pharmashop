<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Image
 *
 * @property int $id
 * @property string|null $url
 * @property string $thumbUrl
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereThumbUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereUrl($value)
 * @mixin \Eloquent
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereName($value)
 */
class Image extends Model
{

}
