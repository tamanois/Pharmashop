<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Article
 *
 * @property int $id
 * @property int $id_user
 * @property string $title
 * @property string $summary
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Article extends Model
{
    public function author()
    {
        return $this->belongsTo(User::class,'id_user');
    }
}
