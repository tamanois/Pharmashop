<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Faq
 *
 * @property int $id
 * @property int $id_faq_category
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Faq_category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereIdFaqCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Faq extends Model
{
    public function category()
    {
        return $this->belongsTo(Faq_category::class,'id_faq_category');
    }
}
