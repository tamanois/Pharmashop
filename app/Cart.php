<?php

namespace pharmashop;

use Illuminate\Database\Eloquent\Model;

/**
 * pharmashop\Cart
 *
 * @property int $id
 * @property int $id_user
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Cart_product[] $cart_products
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float|null $id_country
 * @property float $discount
 * @property int $id_shipping_address
 * @property int $id_billing_address
 * @property string|null $email
 * @property-read \pharmashop\Billing $billing
 * @property-read \pharmashop\Country|null $country
 * @property-read \pharmashop\Shipping $shipping
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdBillingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdShippingAddress($value)
 * @property int $server_order_id
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereServerOrderId($value)
 */
class Cart extends Model
{
    public function user()
    {

        return $this->belongsTo(User::class,'id_user');
    }

    public function cart_products()
    {
        return $this->hasMany(Cart_product::class,'id_cart');
    }

    public function billing()
    {

        return $this->belongsTo(Billing::class,'id_billing_address');
    }

    public function shipping()
    {

        return $this->belongsTo(Shipping::class,'id_shipping_address');
    }

    public function country()
    {

        return $this->belongsTo(Country::class,'id_country');
    }
}
