<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace pharmashop{
/**
 * pharmashop\Article
 *
 * @property int $id
 * @property int $id_user
 * @property string $title
 * @property string $summary
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Article whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Article extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Billing
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_country
 * @property string $address
 * @property string $city
 * @property string $postal_code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Country $country
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Billing whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Billing extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Cart
 *
 * @property int $id
 * @property int $id_user
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Cart_product[] $cart_products
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float|null $id_country
 * @property float $discount
 * @property int $id_shipping_address
 * @property int $id_billing_address
 * @property string|null $email
 * @property-read \pharmashop\Billing $billing
 * @property-read \pharmashop\Country|null $country
 * @property-read \pharmashop\Shipping $shipping
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdBillingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereIdShippingAddress($value)
 * @property int $server_order_id
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart whereServerOrderId($value)
 */
	class Cart extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Cart_product
 *
 * @property int $id
 * @property int $id_cart
 * @property int $id_dosage
 * @property int $quantity
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Cart $cart
 * @property-read \pharmashop\Dosage $dosage
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereIdCart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereIdDosage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Cart_product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Cart_product extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Category
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Category extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Country
 *
 * @property int $id
 * @property string $name
 * @property int $ship
 * @property float $ship_price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Billing[] $billings
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Shipping[] $shippings
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereShip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereShipPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Country whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Country extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Dosage
 *
 * @property int $id
 * @property int $id_product
 * @property string|null $description
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Dosage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Dosage extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Faq
 *
 * @property int $id
 * @property int $id_faq_category
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Faq_category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereIdFaqCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Faq extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Faq_category
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Faq[] $faqs
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Faq_category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Faq_category extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Image
 *
 * @property int $id
 * @property string|null $url
 * @property string $thumbUrl
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereThumbUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereUrl($value)
 * @mixin \Eloquent
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Image whereName($value)
 */
	class Image extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Notification
 *
 * @property int $id
 * @property string|null $message
 * @property int $type
 * @property int $dismissed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereDismissed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Notification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Notification extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Order
 *
 * @property-read \pharmashop\Billing $billing
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\OrderItem[] $order_items
 * @property-read \pharmashop\Shipping $shipping
 * @property-read \pharmashop\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $id_user
 * @property string|null $server_order_id
 * @property float $discount
 * @property int $id_shipping_address
 * @property int $id_billing_address
 * @property string|null $email
 * @property string|null $currency
 * @property float $pay_amount
 * @property int $id_cart
 * @property float $amount
 * @property string|null $payment_url
 * @property int $status
 * @property int $checked
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereIdBillingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereIdCart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereIdShippingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order wherePayAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order wherePaymentUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereServerOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Order whereUpdatedAt($value)
 */
	class Order extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\OrderItem
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $id_order
 * @property int $id_dosage
 * @property float $price
 * @property int $quantity
 * @property float $discount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Dosage $dosage
 * @property-read \pharmashop\Order $order
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereIdDosage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereIdOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\OrderItem whereUpdatedAt($value)
 */
	class OrderItem extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Parametres
 *
 * @property int $id
 * @property string $name
 * @property string|null $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Parametres whereValue($value)
 * @mixin \Eloquent
 */
	class Parametres extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Policy
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Policy whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Policy extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Product
 *
 * @property int $id
 * @property string $name
 * @property string|null $generic_name
 * @property string|null $description
 * @property string|null $imgurl
 * @property int|null $id_category
 * @property int $feature
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Dosage[] $dosages
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereFeature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereGenericName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereIdCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereImgurl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Product extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Review
 *
 * @property int $id
 * @property int $id_user
 * @property float $note
 * @property string $comment
 * @property int $id_product
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Product $product
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Review whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Review extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\Shipping
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_country
 * @property string $address
 * @property string $city
 * @property string $postal_code
 * @property string $phone
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \pharmashop\Country $country
 * @property-read \pharmashop\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\Shipping whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Shipping extends \Eloquent {}
}

namespace pharmashop{
/**
 * pharmashop\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $forname
 * @property string $email
 * @property string $password
 * @property string $level
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\pharmashop\Article[] $articles
 * @property-read \pharmashop\Billing $billing
 * @property-read \pharmashop\Cart $carts
 * @property-read \pharmashop\Review $reviews
 * @property-read \pharmashop\Shipping $shipping
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereForname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\pharmashop\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \pharmashop\Order $orders
 */
	class User extends \Eloquent {}
}

